/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities.api {
public interface IPosition extends IEmployeesCounter, ISelectableEntity{
    function get nID():int;
    function get sName():String;
    function get sDescription():String;
}
}
