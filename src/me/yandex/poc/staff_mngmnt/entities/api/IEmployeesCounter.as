/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 12:52
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities.api {
public interface IEmployeesCounter {
    function get nEmployeesQuantity():int;
    function set nEmployeesQuantity(value:int):void;
}
}
