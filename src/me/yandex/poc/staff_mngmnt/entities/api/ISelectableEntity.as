/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities.api {
public interface ISelectableEntity {
    function get selected():Boolean;
    function set selected(value:Boolean):void;
}
}
