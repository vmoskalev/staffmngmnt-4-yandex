/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities.api {
public interface IEmployee extends ISelectableEntity{
    function get nID():int;
    function get sFirstName():String;
    function get sLastName():String;

    function get infoDepartment():IDepartment;
    function set infoDepartment(value:IDepartment):void;

    function get infoPosition():IPosition;
    function set infoPosition(value:IPosition):void;
}
}
