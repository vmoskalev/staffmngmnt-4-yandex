/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 10:54
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities {
import flash.events.Event;
import flash.events.EventDispatcher;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;
import me.yandex.poc.staff_mngmnt.entities.api.IEmployee;
import me.yandex.poc.staff_mngmnt.entities.api.IPosition;

public class Employee extends EventDispatcher implements IEmployee {
    private var _selected:Boolean;

    private var _nID:int;
    private var _sFirstName:String;
    private var _sLastName:String;

    private var _infoDepartment:IDepartment;
    private var _infoPosition:IPosition;

    [Bindable]
    public function get selected():Boolean {
        return _selected;
    }

    public function set selected(value:Boolean):void {
        if (_selected == value) return;
        _selected = value;
    }

    public function Employee(nID:int, sFirstName:String, sLastName:String) {
        _nID = nID;
        _sFirstName = sFirstName;
        _sLastName = sLastName;
    }


    [Bindable]
    public function get nID():int {
        return _nID;
    }

    public function set nID(value:int):void {
        _nID = value;
    }

    [Bindable]
    public function get sFirstName():String {
        return _sFirstName;
    }

    public function set sFirstName(value:String):void {
        _sFirstName = value;
    }

    [Bindable]
    public function get sLastName():String {
        return _sLastName;
    }

    public function set sLastName(value:String):void {
        _sLastName = value;
    }

    [Bindable]
    public function get infoDepartment():IDepartment {
        return _infoDepartment;
    }

    public function set infoDepartment(value:IDepartment):void {
        _infoDepartment = value;
    }

    [Bindable]
    public function get infoPosition():IPosition {
        return _infoPosition;
    }

    public function set infoPosition(value:IPosition):void {
        _infoPosition = value;
    }

    override public function toString():String {
        return nID.toString();
    }
}
}
