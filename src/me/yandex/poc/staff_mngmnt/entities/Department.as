/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.entities {

import flash.events.Event;
import flash.events.EventDispatcher;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;

public class Department extends EventDispatcher implements IDepartment {
    private var _selected:Boolean;

    private var _nID:int;
    private var _sName:String;
    private var _nEmployeeQuantity:int;

    [Bindable]
    public function get selected():Boolean {
        return _selected;
    }

    public function set selected(value:Boolean):void {
        if (_selected == value) return;
        _selected = value;
    }


    public function Department(nID:int, sName:String, nEmployeeQuantity:int = 0) {
        this.nEmployeesQuantity = nEmployeeQuantity;
        _nID = nID;
        _sName = sName;
        _nEmployeeQuantity = 0;
    }

    [Bindable]
    public function get nID():int {
        return _nID;
    }

    public function set nID(value:int):void {
        _nID = value;
    }

    [Bindable]
    public function get sName():String {
        return _sName;
    }

    public function set sName(value:String):void {
        _sName = value;
    }

    [Bindable]
    public function get nEmployeesQuantity():int {
        return _nEmployeeQuantity;
    }

    public function set nEmployeesQuantity(value:int):void {
        if (_nEmployeeQuantity == value) return;
        _nEmployeeQuantity = value;
    }

    override public function toString():String {
        return sName;
    }
}
}
