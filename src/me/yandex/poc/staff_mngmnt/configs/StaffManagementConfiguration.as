/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.configs {

import me.yandex.poc.staff_mngmnt.StaffMngmnt;
import me.yandex.poc.staff_mngmnt.commands.BrowseAndLoadDatabase;
import me.yandex.poc.staff_mngmnt.commands.ShowDialog;
import me.yandex.poc.staff_mngmnt.commands.sqlite.DepartmentAbolish;
import me.yandex.poc.staff_mngmnt.commands.sqlite.DepartmentEstablish;
import me.yandex.poc.staff_mngmnt.commands.sqlite.EmployeeHire;
import me.yandex.poc.staff_mngmnt.commands.sqlite.EmployeesRequest;

import me.yandex.poc.staff_mngmnt.commands.sqlite.EmployeesTransfer;
import me.yandex.poc.staff_mngmnt.commands.sqlite.InitializeDepartments;
import me.yandex.poc.staff_mngmnt.commands.sqlite.InitializePositions;
import me.yandex.poc.staff_mngmnt.commands.UpdateQuantities;
import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;
import me.yandex.poc.staff_mngmnt.events.EmployeeFilterEvent;
import me.yandex.poc.staff_mngmnt.events.EmployeeHireEvent;
import me.yandex.poc.staff_mngmnt.events.ShowDialogEvent;
import me.yandex.poc.staff_mngmnt.services.SQLiteWorkerService;
import me.yandex.poc.staff_mngmnt.commands.StaffManagerClosing;

import me.yandex.poc.staff_mngmnt.commands.StaffManagerStart;
import me.yandex.poc.staff_mngmnt.commands.sqlite.UpdateEmployeesQuantityInDepartment;
import me.yandex.poc.staff_mngmnt.commands.sqlite.UpdateEmployeesQuantityInPosition;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.TransferEvent;
import me.yandex.poc.staff_mngmnt.events.UpdateQuantityEvent;
import me.yandex.poc.staff_mngmnt.events.UserEvent;
import me.yandex.poc.staff_mngmnt.model.StaffModel;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.services.api.ISQLiteWorkerService;
import me.yandex.poc.staff_mngmnt.utils.EasyLumber;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IAbolishView;
import me.yandex.poc.staff_mngmnt.view.api.IEstablishView;
import me.yandex.poc.staff_mngmnt.view.api.IFilterView;
import me.yandex.poc.staff_mngmnt.view.api.IHireView;
import me.yandex.poc.staff_mngmnt.view.api.IMainView;
import me.yandex.poc.staff_mngmnt.view.api.ITransferView;
import me.yandex.poc.staff_mngmnt.view.api.IWelcomeView;
import me.yandex.poc.staff_mngmnt.view.mediators.AbolishViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.EstablishViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.FilterViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.HireViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.MainViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.TransferViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.WelcomeViewMediator;
import me.yandex.poc.staff_mngmnt.view.mediators.WindowedApplicationMediator;

import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.extensions.viewProcessorMap.api.IViewProcessorMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IInjector;

public class StaffManagementConfiguration implements IConfig {
    [Inject]
    public var injector:IInjector;

    [Inject]
    public var mediatorMap:IMediatorMap;

    [Inject]
    public var commandMap:IEventCommandMap;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var viewProcessorMap:IViewProcessorMap;

    public function configure():void {
        injector.map(IStaffModel).toSingleton(StaffModel);
	    injector.map(ILumber).toSingleton(EasyLumber);

        injector.map(ISQLiteWorkerService).toSingleton(SQLiteWorkerService);

	    mediatorMap.map(StaffMngmnt).toMediator(WindowedApplicationMediator);

        mediatorMap.map(IMainView).toMediator(MainViewMediator);
        mediatorMap.map(IWelcomeView).toMediator(WelcomeViewMediator);
        mediatorMap.map(ITransferView).toMediator(TransferViewMediator);
        mediatorMap.map(IHireView).toMediator(HireViewMediator);
        mediatorMap.map(IFilterView).toMediator(FilterViewMediator);
        mediatorMap.map(IEstablishView).toMediator(EstablishViewMediator);
        mediatorMap.map(IAbolishView).toMediator(AbolishViewMediator);

        commandMap.map(UpdateQuantityEvent.UPDATE_QUANTITY_EVERYWHERE).toCommand(UpdateQuantities);
        commandMap.map(UpdateQuantityEvent.UPDATE_QUANTITY_IN_DEPARTMENT).toCommand(UpdateEmployeesQuantityInDepartment);
        commandMap.map(UpdateQuantityEvent.UPDATE_QUANTITY_IN_POSITION).toCommand(UpdateEmployeesQuantityInPosition);

        commandMap.map(InternalEvent.DEPARTMENTS_UPDATE).toCommand(InitializeDepartments);
        commandMap.map(InternalEvent.CONNECTION_ESTABLISHED).toCommand(InitializeDepartments);
        commandMap.map(InternalEvent.CONNECTION_ESTABLISHED).toCommand(InitializePositions);
        commandMap.map(InternalEvent.ENVIRONMENT_READY).toCommand(UpdateQuantities);
        commandMap.map(InternalEvent.EMPLOYEES_TRANSFER_COMPLETE).toCommand(UpdateQuantities);

	    commandMap.map(UserEvent.MAIN_VIEW_READY).toCommand(StaffManagerStart);
        commandMap.map(UserEvent.APPLICATION_CLOSING).toCommand(StaffManagerClosing);
        commandMap.map(UserEvent.BROWSE_FOR_DATABASE).toCommand(BrowseAndLoadDatabase);

        commandMap.map(ShowDialogEvent.SHOW_DIALOG).toCommand(ShowDialog);

        commandMap.map(DepartmentEvent.ESTABLISH).toCommand(DepartmentEstablish);
        commandMap.map(DepartmentEvent.ABOLISH).toCommand(DepartmentAbolish);
        commandMap.map(EmployeeHireEvent.HIRE).toCommand(EmployeeHire);
        commandMap.map(EmployeeFilterEvent.FILTER_EMPLOYEES).toCommand(EmployeesRequest);
        commandMap.map(TransferEvent.EMPLOYEES_TRANSFER).toCommand(EmployeesTransfer);
    }
}
}
