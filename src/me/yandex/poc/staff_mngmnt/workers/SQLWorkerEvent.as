/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.workers {
import flash.data.SQLResult;
import flash.events.Event;

public class SQLWorkerEvent extends Event {
    public static const RESULT:String = "worker_command_result";
    public static const COMMIT:String = "worker_command_commit";
    public static const REJECT:String = "worker_command_reject";
    public static const BEGIN:String = "worker_command_begin";

    public var uid:String;
    public var token:String;
    public var result:SQLResult;

    public function SQLWorkerEvent(type:String, uid:String, result:SQLResult, token:String = "") {
        super(type, false, false);

        this.uid = uid;
        this.token = token;
        this.result = result;
    }
}
}
