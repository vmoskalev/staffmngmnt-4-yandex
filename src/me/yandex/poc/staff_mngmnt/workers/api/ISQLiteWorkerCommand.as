/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 8:32
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.workers.api {
import flash.events.IEventDispatcher;

public interface ISQLiteWorkerCommand extends IEventDispatcher{
    function execute():void;
}
}
