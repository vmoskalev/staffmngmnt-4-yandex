/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 8:35
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.workers {
import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.EventDispatcher;
import flash.events.SQLEvent;
import flash.net.Responder;

import me.yandex.poc.staff_mngmnt.workers.api.ISQLiteWorkerCommand;

public class SQLiteWorkerTransactionCommand extends EventDispatcher implements ISQLiteWorkerCommand {
    private var _uid:String;
    private var _request:String;
    private var _statement:SQLStatement;
    private var _connection:SQLConnection;
    private var _transaction:Array;

    public function SQLiteWorkerTransactionCommand(uid:String, request:Array, conn:SQLConnection) {
        _uid = uid;
        _connection = conn;
        _transaction = request;

        _statement = new SQLStatement();
//        _statement.addEventListener(SQLEvent.RESULT, resultHandler);
        _statement.sqlConnection = _connection;

    }

    private function executeNext():void {
        if (_transaction.length == 0) {
            trace("[WORKER] SQLiteWorkerTransactionCommand executeNext » commit");
            dispatchEvent(new SQLWorkerEvent(SQLWorkerEvent.COMMIT, _uid, null));
//            _connection.commit();
            return;
        }

        _request = _transaction.shift();
        trace("[WORKER] SQLiteWorkerTransactionCommand executeNext " + _request);
        _statement.text = _request;
        _statement.execute(-1, new Responder(resultHandler));
    }

    public function execute():void {
//        _connection.addEventListener(SQLEvent.BEGIN, beginHandler);
//        _connection.begin();
        executeNext();
    }

    private function resultHandler(result:SQLResult):void {
        trace("[WORKER] SQLiteWorkerTransactionCommand resultHandler " + result.data);
        dispatchEvent(new SQLWorkerEvent(SQLWorkerEvent.RESULT, _uid, result, ""));
        executeNext();
    }

//    private function beginHandler(event:SQLEvent):void {
//        trace("[WORKER] SQLiteWorkerTransactionCommand beginHandler");
//        dispatchEvent(new SQLWorkerEvent(SQLWorkerEvent.BEGIN, "", null));
//        _connection.removeEventListener(SQLEvent.BEGIN, beginHandler);
//        _connection.addEventListener(SQLEvent.COMMIT, commitHandler);
//        executeNext();
//    }

    private function commitHandler(event:SQLEvent):void {
        trace("[WORKER] SQLiteWorkerTransactionCommand commitHandler");
        _connection.removeEventListener(SQLEvent.COMMIT, commitHandler);
        dispatchEvent(new SQLWorkerEvent(SQLWorkerEvent.COMMIT, _uid, null));
    }
}
}
