/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 12:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.workers {
import flash.data.SQLConnection;
import flash.data.SQLMode;
import flash.display.Sprite;
import flash.events.SQLEvent;
import flash.events.TimerEvent;
import flash.filesystem.File;
import flash.system.MessageChannel;
import flash.system.Worker;
import flash.utils.ByteArray;
import flash.utils.Timer;
import flash.utils.getTimer;

import me.yandex.poc.staff_mngmnt.workers.api.ISQLiteWorkerCommand;

public class SQLiteWorker extends Sprite {
    private var _gateway:ByteArray = new ByteArray();

    private var _worker2service:MessageChannel;
    private var _service2worker:MessageChannel;
    private var _start:int;

    private var _timer:Timer;

    private var _connection:SQLConnection;

    public function SQLiteWorker() {
        _gateway.shareable = true;
        _timer = new Timer(10);
        _timer.addEventListener(TimerEvent.TIMER, timer_timerHandler);

        _worker2service = Worker.current.getSharedProperty("w2s") as MessageChannel;
        _service2worker = Worker.current.getSharedProperty("s2w") as MessageChannel;
        _start = int(Worker.current.getSharedProperty("startTime"));

        lumber();

        _timer.start();
//        _service2worker.addEventListener(Event.CHANNEL_MESSAGE, service2worker_handler);
        _worker2service.send(_gateway);
    }

    private function connection_openHandler(event:SQLEvent):void {
        lumber();
        _connection.removeEventListener(SQLEvent.OPEN, connection_openHandler);
        _worker2service.send(JSON.stringify({info: "connection_established"}));
    }

    private function timer_timerHandler(event:TimerEvent):void {
        if (!_service2worker.messageAvailable) return;

        processReceivedObject(JSON.parse(_service2worker.receive(true) as String));
    }

    private function processReceivedObject(obj:Object):void {
        var command:ISQLiteWorkerCommand;

        switch (obj.type) {
            case "CLOSE":
                lumber(obj.type);
                _connection.addEventListener(SQLEvent.CLOSE, closeHandler);
                _connection.close();
                break;
            case "FILE":
                lumber(obj.type, obj.path);
                _connection = new SQLConnection();
                _connection.addEventListener(SQLEvent.OPEN, connection_openHandler);
                _connection.openAsync(new File(obj.path), SQLMode.UPDATE);
                break;
            case "TRANSACTION":
                lumber(obj.type, obj.uid, obj.request);
                command = new SQLiteWorkerTransactionCommand(obj.uid, obj.request, _connection);
//                command.addEventListener(SQLWorkerEvent.RESULT, resultHandler);
                command.addEventListener(SQLWorkerEvent.COMMIT, commitHandler);
                command.execute();
                break;
            case "REQUEST":
                lumber(obj.type, obj.uid, obj.request);
                command = new SQLiteWorkerRequestCommand(obj.uid, obj.request[0], _connection);
                command.addEventListener(SQLWorkerEvent.RESULT, resultHandler);
                command.execute();
                break;
            default:
                lumber("wtf?");
        }
    }

    private function get caller():String {
        return (new Error()).getStackTrace().split("at ")[3].split("::")[1].split("(")[0].replace("/", " » ");
    }

    private function lumber(...args):void {
        trace(timeDiff, "[WORKER]\t", caller, args);
    }

    private function get timeDiff():String {
        return "" + (getTimer() + _start).toString() + "\t";
    }

    private function closeHandler(event:SQLEvent):void {
        _connection.removeEventListener(SQLEvent.CLOSE, closeHandler);
        _worker2service.send(JSON.stringify({info: "connection_closed"}));
    }

    private function resultHandler(event:SQLWorkerEvent):void {
        var cmnd:SQLiteWorkerRequestCommand = event.target as SQLiteWorkerRequestCommand;
        if (!cmnd) return;

        cmnd.removeEventListener(SQLWorkerEvent.RESULT, resultHandler);
        _gateway.length = 0;

//        lumber(event.result.data);

        _gateway.writeObject(event.result.data ? event.result.data : []);
        _worker2service.send(JSON.stringify({info: "result", type: event.uid, token: event.token}));
    }

    private function commitHandler(event:SQLWorkerEvent):void {
        var cmnd:SQLiteWorkerTransactionCommand = event.target as SQLiteWorkerTransactionCommand;
        if (!cmnd) return;
        cmnd.removeEventListener(SQLWorkerEvent.COMMIT, resultHandler);

        lumber();
        _worker2service.send(JSON.stringify({info: "commit", type: event.uid, token: event.token}));
    }
}
}
