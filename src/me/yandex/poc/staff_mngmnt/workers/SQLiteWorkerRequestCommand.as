/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.workers {
import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.EventDispatcher;
import flash.events.SQLEvent;

import me.yandex.poc.staff_mngmnt.workers.api.ISQLiteWorkerCommand;

public class SQLiteWorkerRequestCommand extends EventDispatcher implements ISQLiteWorkerCommand{
    private var _uid:String;
    private var _statement:SQLStatement;

    public function SQLiteWorkerRequestCommand(uid:String, request:String, conn:SQLConnection) {
        _uid = uid;

        _statement = new SQLStatement();
        _statement.addEventListener(SQLEvent.RESULT, resultHandler);
        _statement.sqlConnection = conn;
        _statement.text = request;
    }

    public function execute():void {
        _statement.execute();
    }

    private function resultHandler(event:SQLEvent):void {
        _statement.removeEventListener(SQLEvent.RESULT, resultHandler);
        var result:SQLResult = _statement.getResult();
        dispatchEvent(new SQLWorkerEvent(SQLWorkerEvent.RESULT,_uid, result));
    }
}
}
