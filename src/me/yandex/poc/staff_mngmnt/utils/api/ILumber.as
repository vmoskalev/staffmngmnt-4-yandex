/**
 * Created with IntelliJ IDEA.
 * User: vmoskalev
 * Date: 09.08.13
 * Time: 9:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.utils.api {
public interface ILumber {
	function info(... args):void;
	function fail(... args):void;
    function get startTime():int;
}
}
