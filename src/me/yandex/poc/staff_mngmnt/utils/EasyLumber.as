/**
 * Created with IntelliJ IDEA.
 * User: vmoskalev
 * Date: 09.08.13
 * Time: 9:19
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.utils {
import flash.utils.getTimer;

import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

public class EasyLumber implements ILumber{
    private static const _start:int = getTimer();
    private function get caller():String {
        return (new Error()).getStackTrace().split("at ")[3].split("::")[1].split("(")[0].replace("/"," » ");
    }

    public function info(... args):void {
		trace(timeDiff,"[INFO]\t\t", caller, args);
	}

	public function fail(... args):void {
		trace(timeDiff,"[FAIL]\t\t", caller, args);
	}

    public function get startTime():int {
        return _start;
    }

    private function get timeDiff():String {
        return "" + (getTimer() - _start).toString() + "\t";
    }
}
}
