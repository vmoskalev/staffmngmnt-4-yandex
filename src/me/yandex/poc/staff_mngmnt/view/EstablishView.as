/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;

import me.yandex.poc.staff_mngmnt.view.api.IEstablishView;
import me.yandex.poc.staff_mngmnt.view.base.DialogView;

import mx.managers.PopUpManager;

import spark.components.supportClasses.ButtonBase;

import spark.components.supportClasses.SkinnableComponent;
import spark.components.supportClasses.SkinnableTextBase;

public class EstablishView extends DialogView implements IEstablishView {
    [SkinPart]
    public var deptName:SkinnableTextBase;

    [SkinPart]
    public var btnEstablish:ButtonBase;

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (instance == btnEstablish) {
            btnEstablish.addEventListener(MouseEvent.CLICK, btnEstablish_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (instance == btnEstablish) {
            btnEstablish.removeEventListener(MouseEvent.CLICK, btnEstablish_clickHandler);
        }
    }

    private function btnEstablish_clickHandler(event:MouseEvent):void {
        dispatchEvent(new DepartmentEvent(DepartmentEvent.ESTABLISH, deptName.text));
    }

}
}
