/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 12.08.13
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.EmployeeHireEvent;

import me.yandex.poc.staff_mngmnt.view.api.IHireView;
import me.yandex.poc.staff_mngmnt.view.base.DialogView;

import mx.collections.IList;

import spark.components.supportClasses.ButtonBase;
import spark.components.supportClasses.SkinnableTextBase;

public class HireView extends DialogView implements IHireView {
    [SkinPart(required="true")]
    public var btnHire:ButtonBase;

    [SkinPart(required="true")]
    public var textFirstName:SkinnableTextBase;

    [SkinPart(required="true")]
    public var textLastName:SkinnableTextBase;

    private function get lastName():String {
        return textLastName.text;
    }

    private function get firstName():String {
        return textFirstName.text;
    }

    private function get position():String {
        return listPositions ? listPositions.selectedItem.sName : "";
    }

    private function get department():int {
        return listDepartments ? listDepartments.selectedItem.nID : -1;
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (btnHire == instance) {
            btnHire.addEventListener(MouseEvent.CLICK, btnHire_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (btnHire == instance) {
            btnHire.removeEventListener(MouseEvent.CLICK, btnHire_clickHandler);
        }
    }

    public function set providerFirstNames(value:IList):void {
    }

    public function set providerLastNames(value:IList):void {
    }

    private function btnHire_clickHandler(event:MouseEvent):void {
        dispatchEvent(new EmployeeHireEvent(department, firstName, lastName, position));
    }
}
}
