/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.Event;
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;

import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;

import me.yandex.poc.staff_mngmnt.view.api.IAbolishView;
import me.yandex.poc.staff_mngmnt.view.base.DialogView;

import mx.controls.Alert;

import mx.controls.listClasses.ListBase;

import mx.events.CloseEvent;

import spark.components.supportClasses.ButtonBase;

[SkinState("promptTransfer")]
public class AbolishView extends DialogView implements IAbolishView {
    [SkinPart(required="true")]
    public var btnAbolish:ButtonBase;

    [SkinPart(required="false")]
    public var btnTransfer:ButtonBase;

    [SkinPart(required="false")]
    public var listTransferTarget:ListBase;

    override protected function commitDepartments():void {
        if (_providerDepartmentsChanged && (listDepartments || listTransferTarget)) {
            _providerDepartmentsChanged = false;

            if (listDepartments)
                listDepartments.dataProvider = _providerDepartments;
            if (listTransferTarget)
                listTransferTarget.dataProvider = _providerDepartments;
        }
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (instance == btnAbolish) {
            btnAbolish.addEventListener(MouseEvent.CLICK, btnAbolish_clickHandler);
        } else if (instance == btnTransfer) {
            btnTransfer.addEventListener(Event.CLEAR, btnTransfer_clearHandler);
        } else if (instance == listTransferTarget) {
            listTransferTarget.dataProvider = _providerDepartments;
        }
    }

    private function btnAbolish_clickHandler(event:MouseEvent):void {
        dispatchEvent(new DepartmentEvent(DepartmentEvent.ABOLISH));

//        Alert.yesLabel = "Увольянем";
//        Alert.noLabel = "Переводим";
//        Alert.show("Что с сотрудниками делать будем?", "А сотрудники?", Alert.YES | Alert.NO, null, onAlertResponse);
    }

    private function btnTransfer_clearHandler(event:Event):void {
        var selectedDept:IDepartment = listTransferTarget.selectedItem as IDepartment;
        if (!selectedDept) throw new Error("something wrong");

        for each (var dept:IDepartment in _providerDepartments) {
            if (dept.selected && dept == selectedDept) {
                Alert.show("Вы хотите перевести в удаляемый отдел", "Упс");
                return;
            }
        }

        dispatchEvent(new DepartmentEvent(DepartmentEvent.ABOLISH, listTransferTarget.selectedItem.sName));
    }

    private function onAlertResponse(event:CloseEvent):void {
        if (event.detail == Alert.NO) {
            skin.currentState = "promptTransfer";
        } else {
            dispatchEvent(new DepartmentEvent(DepartmentEvent.ABOLISH));
        }
    }
}
}
