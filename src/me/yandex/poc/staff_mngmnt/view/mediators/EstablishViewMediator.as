/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IEstablishView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class EstablishViewMediator extends Mediator {
    [Inject]
    public var view:IEstablishView;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var model:IStaffModel;

    override public function initialize():void {
        lumber.info();

        addViewListener(DepartmentEvent.ESTABLISH, dispatch);
        addViewListener(InternalEvent.HIDE_DIALOG, dispatch);

        addContextListener(InternalEvent.DEPARTMENT_ESTABLISH_BEGIN, beginHandler);
        addContextListener(InternalEvent.DEPARTMENT_ESTABLISH_COMPLETE, completeHandler);
    }

    override public function destroy():void {
        lumber.info();

        removeViewListener(DepartmentEvent.ESTABLISH, dispatch);
        removeViewListener(InternalEvent.HIDE_DIALOG, dispatch);

        removeContextListener(InternalEvent.DEPARTMENT_ESTABLISH_BEGIN, beginHandler);
        removeContextListener(InternalEvent.DEPARTMENT_ESTABLISH_COMPLETE, completeHandler);

    }

    private function beginHandler(event:InternalEvent):void {
        view.showProcess();
    }

    private function completeHandler(event:InternalEvent):void {
        view.showSuccess();
    }
}
}
