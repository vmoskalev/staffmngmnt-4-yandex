/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 17:10
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.TransferEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.ITransferView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class TransferViewMediator extends Mediator {
    [Inject]
    public var view:ITransferView;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var model:IStaffModel;

    override public function initialize():void {
        lumber.info();

        addViewListener(TransferEvent.EMPLOYEES_TRANSFER, dispatch);
        addViewListener(InternalEvent.HIDE_DIALOG, dispatch);

        addContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        addContextListener(InternalEvent.EMPLOYEES_READY, onEmployeesReady);

        addContextListener(InternalEvent.EMPLOYEES_TRANSFER_BEGIN, onEmployeesTransferBegin);
        addContextListener(InternalEvent.EMPLOYEES_TRANSFER_COMMIT, onEmployeesTransferCommit);
        addContextListener(InternalEvent.EMPLOYEES_TRANSFER_COMPLETE, onEmployeesTransferComplete);

        view.providerDepartments = model.listDepartments;
        view.providerEmployees = model.listEmployees;
    }

    private function onEmployeesTransferCommit(event:InternalEvent):void {
//        view.showSuccess();
    }

    private function onEmployeesTransferComplete(event:InternalEvent):void {
        view.showSuccess();
    }

    private function onEmployeesTransferBegin(event:InternalEvent):void {
        view.showProcess();
    }

    override public function destroy():void {
        lumber.info();

        removeViewListener(InternalEvent.HIDE_DIALOG, dispatch);
        removeViewListener(TransferEvent.EMPLOYEES_TRANSFER, dispatch);

        removeContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        removeContextListener(InternalEvent.EMPLOYEES_READY, onEmployeesReady);

        removeContextListener(InternalEvent.EMPLOYEES_TRANSFER_BEGIN, onEmployeesTransferBegin);
        removeContextListener(InternalEvent.EMPLOYEES_TRANSFER_COMMIT, onEmployeesTransferCommit);
        removeContextListener(InternalEvent.EMPLOYEES_TRANSFER_COMPLETE, onEmployeesTransferComplete);

    }

    private function onDepartmentsReady(event:InternalEvent):void {
        lumber.info();
        view.providerDepartments = model.listDepartments;
    }

    private function onEmployeesReady(event:InternalEvent):void {
        lumber.info();
        view.providerEmployees = model.listEmployees;
    }
}
}
