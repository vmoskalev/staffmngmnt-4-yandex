/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 13:40
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IAbolishView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class AbolishViewMediator extends Mediator {
    [Inject]
    public var view:IAbolishView;

    [Inject]
    public var model:IStaffModel;

    [Inject]
    public var lumber:ILumber;

    override public function initialize():void {
        lumber.info();
        addViewListener(InternalEvent.HIDE_DIALOG, dispatch);
        addViewListener(DepartmentEvent.ABOLISH, dispatch);

        addContextListener(InternalEvent.DEPARTMENT_ABOLISH_BEGIN, beginHandler);
        addContextListener(InternalEvent.DEPARTMENT_ABOLISH_COMPLETE, completeHandler);

        view.providerDepartments = model.listDepartments;
    }

    override public function destroy():void {
        lumber.info();
        removeViewListener(InternalEvent.HIDE_DIALOG, dispatch);
        removeViewListener(DepartmentEvent.ABOLISH, dispatch);
    }

    private function beginHandler(event:InternalEvent):void {
        view.showProcess();
    }

    private function completeHandler(event:InternalEvent):void {
        view.showSuccess();
    }
}
}
