/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 19.08.13
 * Time: 20:44
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.EmployeeFilterEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.UserEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IFilterView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class FilterViewMediator extends Mediator {
    [Inject]
    public var view:IFilterView;

    [Inject]
    public var model:IStaffModel;

    [Inject]
    public var lumber:ILumber;

    override public function initialize():void {
        lumber.info();
        addContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        addContextListener(InternalEvent.POSITIONS_READY, onPositionsReady);

        addContextListener(InternalEvent.EMPLOYEES_REQUEST_BEGIN, onEmployeesRequestBegin);
        addContextListener(InternalEvent.EMPLOYEES_REQUEST_COMPLETE, onEmployeesRequestComplete);
        addContextListener(InternalEvent.EMPLOYEES_REQUEST_FAULT, onEmployeesRequestFault);

        view.providerDepartments = model.listDepartments;
        view.providerPositions = model.listPositions;
//        view.providerEmployees = model.listEmployees;

        addViewListener(EmployeeFilterEvent.FILTER_EMPLOYEES, dispatch);
        addViewListener(InternalEvent.HIDE_DIALOG, dispatch);
    }

    override public function destroy():void {
        lumber.info();

        removeContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        removeContextListener(InternalEvent.POSITIONS_READY, onPositionsReady);

        removeContextListener(InternalEvent.EMPLOYEES_REQUEST_BEGIN, onEmployeesRequestBegin);
        removeContextListener(InternalEvent.EMPLOYEES_REQUEST_COMPLETE, onEmployeesRequestComplete);
        removeContextListener(InternalEvent.EMPLOYEES_REQUEST_FAULT, onEmployeesRequestFault);

        removeViewListener(UserEvent.APPLICATION_CLOSING, dispatch);
        removeViewListener(InternalEvent.HIDE_DIALOG, dispatch);
    }

    private function onDepartmentsReady(event:InternalEvent):void {
        lumber.info();
        view.providerDepartments = model.listDepartments;
    }

    private function onPositionsReady(event:InternalEvent):void {
        lumber.info();
        view.providerPositions = model.listPositions;
    }

    private function onEmployeesRequestComplete(event:InternalEvent):void {
        lumber.info();
        view.showSuccess();
    }
    private function onEmployeesRequestFault(event:InternalEvent):void {
        view.showFault();
    }

    private function onEmployeesRequestBegin(event:InternalEvent):void {
        lumber.info();
        view.showProcess();
    }
}
}
