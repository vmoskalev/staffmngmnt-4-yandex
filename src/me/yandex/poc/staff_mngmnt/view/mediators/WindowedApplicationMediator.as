/**
 * Created with IntelliJ IDEA.
 * User: vmoskalev
 * Date: 12.08.13
 * Time: 9:00
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import flash.events.Event;

import me.yandex.poc.staff_mngmnt.StaffMngmnt;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.UserEvent;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import robotlegs.bender.bundles.mvcs.Mediator;

public class WindowedApplicationMediator extends Mediator {
	[Inject]
	public var view:StaffMngmnt;

	[Inject]
	public var lumber:ILumber;

	override public function initialize():void {
		lumber.info();

		addContextListener(InternalEvent.ENVIRONMENT_READY, startup);
	}

	override public function destroy():void {
		lumber.info();
	}

	private function view_closingHandler(event:Event):void {
		lumber.info();
		event.preventDefault();
		dispatch(new UserEvent(UserEvent.APPLICATION_CLOSING));
	}

	private function shutdown(event:InternalEvent):void {
		lumber.info();
		removeViewListener(Event.CLOSING, view_closingHandler);
		removeContextListener(InternalEvent.CONNECTION_CLOSED, shutdown);

		view.close();
	}

	private function startup(event:InternalEvent):void {
		lumber.info();
		removeContextListener(InternalEvent.ENVIRONMENT_READY, startup);
		addViewListener(Event.CLOSING, view_closingHandler);
		addContextListener(InternalEvent.CONNECTION_CLOSED, shutdown);
	}
}
}
