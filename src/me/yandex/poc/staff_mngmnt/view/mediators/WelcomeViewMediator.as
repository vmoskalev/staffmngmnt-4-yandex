/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.UserEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IWelcomeView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class WelcomeViewMediator extends Mediator {
    [Inject]
    public var view:IWelcomeView;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var model:IStaffModel;

    override public function initialize():void {
        lumber.info();

        addViewListener(UserEvent.BROWSE_FOR_DATABASE, dispatch);
        addViewListener(UserEvent.PROCEED_TO_APPLICATION, onProceedClick);

        addContextListener(InternalEvent.ENVIRONMENT_READY, onEnvironmentReady);
        addContextListener(InternalEvent.CONNECTION_PREPARING, onConnectionBeforeReady);
    }

    private function onEnvironmentReady(event:InternalEvent):void {
        lumber.info();
        view.showPendingState();
    }

    private function onConnectionBeforeReady(event:InternalEvent):void {
        lumber.info();
        view.showLoadingState();
    }

    override public function destroy():void {
        lumber.info();

        removeViewListener(UserEvent.BROWSE_FOR_DATABASE, dispatch);
        removeViewListener(UserEvent.PROCEED_TO_APPLICATION, onProceedClick);

        removeContextListener(InternalEvent.ENVIRONMENT_READY, onEnvironmentReady);
        removeContextListener(InternalEvent.CONNECTION_ESTABLISHED, onConnectionBeforeReady);
    }

    private function onProceedClick(event:UserEvent):void {
        view.hide();
    }
}
}
