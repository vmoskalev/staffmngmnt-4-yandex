/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 12.08.13
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.EmployeeHireEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IHireView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class HireViewMediator extends Mediator {
    [Inject]
    public var view:IHireView;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var model:IStaffModel;

    override public function initialize():void {
        lumber.info();

        addViewListener(EmployeeHireEvent.HIRE, dispatch);
        addViewListener(InternalEvent.HIDE_DIALOG, dispatch);

        addContextListener(InternalEvent.EMPLOYEES_HIRING_BEGIN, hiringBegin_handler);
        addContextListener(InternalEvent.EMPLOYEES_HIRING_COMPLETE, hiringComplete_handler);
        addContextListener(InternalEvent.DEPARTMENTS_READY, departments_readyHandler);
        addContextListener(InternalEvent.POSITIONS_READY, positions_readyHandler);

        view.providerDepartments = model.listDepartments;
        view.providerPositions = model.listPositions;
    }

    override public function destroy():void {
        lumber.info();

        removeViewListener(EmployeeHireEvent.HIRE, dispatch);
        removeViewListener(InternalEvent.HIDE_DIALOG, dispatch);

        removeContextListener(InternalEvent.DEPARTMENTS_READY, departments_readyHandler);
        removeContextListener(InternalEvent.POSITIONS_READY, positions_readyHandler);

    }

    private function departments_readyHandler(event:InternalEvent):void {
        lumber.info();
        view.providerDepartments = model.listDepartments;
    }

    private function positions_readyHandler(event:InternalEvent):void {
        lumber.info();
        view.providerPositions = model.listPositions;
    }

    private function hiringBegin_handler(event:InternalEvent):void {
        view.showProcess();
    }

    private function hiringComplete_handler(event:InternalEvent):void {
        view.showSuccess();
    }
}
}
