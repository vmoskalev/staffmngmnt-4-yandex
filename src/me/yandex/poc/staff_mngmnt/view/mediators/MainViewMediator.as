/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.mediators {
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.ShowDialogEvent;
import me.yandex.poc.staff_mngmnt.events.UserEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.IMainView;

import robotlegs.bender.bundles.mvcs.Mediator;

public class MainViewMediator extends Mediator {
    [Inject]
    public var view:IMainView;

    [Inject]
    public var model:IStaffModel;

	[Inject]
	public var lumber:ILumber;

    override public function initialize():void {
        addContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        addContextListener(InternalEvent.POSITIONS_READY, onPositionsReady);
        addContextListener(InternalEvent.EMPLOYEES_READY, onEmployeesReady);

        addContextListener(InternalEvent.HIDE_DIALOG, onHideDialog);

        view.providerDepartments = model.listDepartments;
        view.providerPositions = model.listPositions;
        view.providerEmployees = model.listEmployees;

        addViewListener(UserEvent.APPLICATION_CLOSING, dispatch);
        addViewListener(ShowDialogEvent.SHOW_DIALOG, dispatch);

//        addViewListener(UserEvent.EMPLOYEES_HIRE, dispatch);
//        addViewListener(UserEvent.EMPLOYEES_REQUEST, dispatch);
//        addViewListener(UserEvent.EMPLOYEES_TRANSFER, dispatch);
//        addViewListener(UserEvent.DEPARTMENT_ABOLISH, dispatch);
//        addViewListener(UserEvent.DEPARTMENT_ESTABLISH, dispatch);
        dispatch(new UserEvent(UserEvent.MAIN_VIEW_READY));
    }

    override public function destroy():void {
        removeContextListener(InternalEvent.DEPARTMENTS_READY, onDepartmentsReady);
        removeContextListener(InternalEvent.POSITIONS_READY, onPositionsReady);
        removeContextListener(InternalEvent.EMPLOYEES_READY, onEmployeesReady);

        removeViewListener(ShowDialogEvent.SHOW_DIALOG, dispatch);
        removeViewListener(UserEvent.APPLICATION_CLOSING, dispatch);

//        removeViewListener(UserEvent.EMPLOYEES_HIRE, dispatch);
//        removeViewListener(UserEvent.EMPLOYEES_TRANSFER, dispatch);
//        removeViewListener(UserEvent.EMPLOYEES_REQUEST, dispatch);
//        removeViewListener(UserEvent.DEPARTMENT_ABOLISH, dispatch);
//        removeViewListener(UserEvent.DEPARTMENT_ESTABLISH, dispatch);
    }

    private function onHideDialog(event:InternalEvent):void {
        lumber.info();
        view.setSkinEnabled();
    }

    private function onDepartmentsReady(event:InternalEvent):void {
        lumber.info();
        view.providerDepartments = model.listDepartments;
    }

    private function onPositionsReady(event:InternalEvent):void {
        lumber.info();
        view.providerPositions = model.listPositions;
    }

    private function onEmployeesReady(event:InternalEvent):void {
        lumber.info();
        view.providerEmployees = model.listEmployees;
    }
}
}
