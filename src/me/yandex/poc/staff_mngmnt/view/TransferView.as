/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.TransferEvent;

import me.yandex.poc.staff_mngmnt.view.api.ITransferView;
import me.yandex.poc.staff_mngmnt.view.base.DialogView;

import mx.collections.IList;
import mx.controls.Alert;
import mx.events.CloseEvent;

import spark.components.supportClasses.ButtonBase;

import spark.components.supportClasses.ListBase;

import spark.components.supportClasses.ToggleButtonBase;

[SkinState("promptToCheck")]
[SkinState("promptToSelect")]

public class TransferView extends DialogView implements ITransferView {
    private var _providerEmployees:IList;
    private var _providerEmployeesChanged:Boolean = false;

    [SkinPart(required="true")]
    public var listEmployees:ListBase;

    [SkinPart(required="true")]
    public var btnTransfer:ButtonBase;

    [SkinPart(required="false")]
    public var radioFire:ToggleButtonBase;

    override protected function commitProperties():void {
        super.commitProperties();
        commitEmployees();
    }

    public function set providerEmployees(value:IList):void {
        if (_providerEmployees != value) {
            _providerEmployees = value;
            _providerEmployeesChanged = true;

            invalidateProperties();
        }
    }

    protected function commitEmployees():void {
        if (_providerEmployeesChanged && listEmployees) {
            _providerEmployeesChanged = false;

            if (listEmployees)
                listEmployees.dataProvider = _providerEmployees;
        }
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (listDepartments == instance) {
            commitDepartments();
        } else if (listEmployees == instance) {
            commitEmployees();
        } else if (btnTransfer == instance) {
            btnTransfer.addEventListener(MouseEvent.CLICK, btnTransfer_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (btnTransfer == instance) {
            btnTransfer.removeEventListener(MouseEvent.CLICK, btnTransfer_clickHandler);
        }
    }

    private function btnTransfer_clickHandler(event:MouseEvent):void {
        Alert.yesLabel = "Да!";
        Alert.noLabel = "Нет..";
        Alert.show("Точно " + (radioFire.selected ? "увольняем?" : "перемещаем?"), "Вы уверены?",Alert.YES|Alert.NO,null,onAlertResponse);
    }

    private function onAlertResponse(event:CloseEvent):void {
        if (event.detail == Alert.YES) {
            dispatchEvent(new TransferEvent(radioFire.selected ? 0 : listDepartments.selectedItem.nID));
        }
    }
}
}
