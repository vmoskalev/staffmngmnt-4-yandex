/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.base {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.InternalEvent;

import me.yandex.poc.staff_mngmnt.view.api.base.IDialogView;

import mx.managers.PopUpManager;

import spark.components.supportClasses.ButtonBase;

[SkinState("prompt")]
[SkinState("processing")]
[SkinState("reportSuccess")]
[SkinState("reportFault")]
public class DialogView extends ProviderHostView implements IDialogView{

    [SkinPart(required="false")]
    public var btnCancel:ButtonBase;

    [SkinPart(required="false")]
    public var btnClose:ButtonBase;

    final public function hide():void {
        dispatchEvent(new InternalEvent(InternalEvent.HIDE_DIALOG));
        PopUpManager.removePopUp(this);
    }

    final public function showProcess():void {
        skin.currentState = "processing";
    }

    final public function showSuccess():void {
        skin.currentState = "reportSuccess";
    }

    final public function showFault():void {
        skin.currentState = "reportFault";
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (btnCancel == instance) {
            btnCancel.addEventListener(MouseEvent.CLICK, btnCancel_clickHandler);
        } else if (btnClose == instance) {
            btnClose.addEventListener(MouseEvent.CLICK, btnClose_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (btnCancel == instance) {
            btnCancel.removeEventListener(MouseEvent.CLICK, btnCancel_clickHandler);
        } else if (btnClose == instance) {
            btnClose.removeEventListener(MouseEvent.CLICK, btnClose_clickHandler);
        }
    }

    protected function btnCancel_clickHandler(event:MouseEvent):void {
        hide();
    }

    protected function btnClose_clickHandler(event:MouseEvent):void {
        hide();
    }
}
}
