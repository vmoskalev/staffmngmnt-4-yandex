/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 11:50
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.base {
import me.yandex.poc.staff_mngmnt.view.api.base.IProviderHostView;

import mx.collections.IList;

import spark.components.supportClasses.ListBase;

import spark.components.supportClasses.SkinnableComponent;

public class ProviderHostView extends SkinnableComponent implements IProviderHostView{
    [SkinPart(required="false")]
    public var listPositions:ListBase;

    [SkinPart(required="false")]
    public var listDepartments:ListBase;

    protected var _providerPositions:IList;
    protected var _providerPositionsChanged:Boolean = false;

    protected var _providerDepartments:IList;
    protected var _providerDepartmentsChanged:Boolean = false;

    public function set providerPositions(value:IList):void {
        if (_providerPositions != value) {
            _providerPositions = value;
            _providerPositionsChanged = true;

            invalidateProperties();
        }
    }

    public function set providerDepartments(value:IList):void {
        if (_providerDepartments != value) {
            _providerDepartments = value;
            _providerDepartmentsChanged = true;

            invalidateProperties();
        }
    }

    protected function commitPositions():void {
        if (_providerPositionsChanged && listPositions) {
            _providerPositionsChanged = false;
            listPositions.dataProvider = _providerPositions;
        }
    }

    protected function commitDepartments():void {
        if (_providerDepartmentsChanged && listDepartments) {
            _providerDepartmentsChanged = false;
            listDepartments.dataProvider = _providerDepartments;
        }
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (listDepartments == instance) {
            listDepartments.dataProvider = _providerDepartments;
        } else if (listPositions == instance) {
            listPositions.dataProvider = _providerPositions;
        }
    }

    override protected function commitProperties():void {
        super.commitProperties();

        commitDepartments();
        commitPositions();
    }
}
}
