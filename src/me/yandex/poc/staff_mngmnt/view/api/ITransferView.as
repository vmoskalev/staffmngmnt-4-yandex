/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api {

import me.yandex.poc.staff_mngmnt.view.api.base.IDialogView;
import me.yandex.poc.staff_mngmnt.view.api.base.IProviderHostView;

import mx.collections.IList;

public interface ITransferView extends IDialogView, IProviderHostView{
    function set providerEmployees(value:IList):void;
}
}
