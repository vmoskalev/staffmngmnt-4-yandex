/**
 * Created with IntelliJ IDEA.
 * User: vmoskalev
 * Date: 09.08.13
 * Time: 8:53
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api {
import flash.events.IEventDispatcher;

import me.yandex.poc.staff_mngmnt.view.api.base.IProviderHostView;

import mx.collections.IList;

public interface IMainView extends IProviderHostView, IEventDispatcher{
	function set providerEmployees(value:IList):void;
    function setSkinEnabled():void;
}
}
