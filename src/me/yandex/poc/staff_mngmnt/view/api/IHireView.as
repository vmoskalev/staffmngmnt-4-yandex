/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 12.08.13
 * Time: 21:15
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api {
import flash.events.IEventDispatcher;

import me.yandex.poc.staff_mngmnt.view.api.base.IDialogView;
import me.yandex.poc.staff_mngmnt.view.api.base.IProviderHostView;

import mx.collections.IList;
import mx.core.IFlexDisplayObject;

public interface IHireView extends IDialogView, IProviderHostView{
    function set providerFirstNames(value:IList):void;
    function set providerLastNames(value:IList):void;
}
}
