/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api.base {
import mx.collections.IList;

public interface IProviderHostView {
    function set providerPositions(value:IList):void;
    function set providerDepartments(value:IList):void;
}
}
