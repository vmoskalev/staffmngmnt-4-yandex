/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 9:56
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api.base {
import flash.events.IEventDispatcher;

import mx.core.IFlexDisplayObject;

public interface IDialogView extends IEventDispatcher, IFlexDisplayObject{
    function hide():void;
    function showProcess():void;
    function showSuccess():void;
    function showFault():void;
}
}
