/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view.api {
import flash.events.IEventDispatcher;

import mx.core.IFlexDisplayObject;
import mx.core.IStateClient;

public interface IWelcomeView extends IEventDispatcher, IFlexDisplayObject {
    function hide():void;
    function showLoadingState():void;
    function showPendingState():void;
}
}
