/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.ShowDialogEvent;

import me.yandex.poc.staff_mngmnt.view.api.IMainView;
import me.yandex.poc.staff_mngmnt.view.base.ProviderHostView;

import mx.collections.IList;

import spark.components.DataGrid;
import spark.components.supportClasses.ButtonBase;

import spark.components.supportClasses.ListBase;
import spark.components.supportClasses.SkinnableComponent;

public class MainView extends ProviderHostView implements IMainView {
    private var _providerEmployees:IList;
    private var _providerEmployeesChanged:Boolean = false;

    [SkinPart(required="false")]
    public var listEmployees:ListBase;

    [SkinPart(required="false")]
    public var gridEmployees:DataGrid;

    [SkinPart(required="true")]
    public var btnEmployeesRequest:ButtonBase;

    [SkinPart(required="true")]
    public var btnDepartmentsEstablish:ButtonBase;

    [SkinPart(required="true")]
    public var btnDepartmentsAbolish:ButtonBase;

    [SkinPart(required="true")]
    public var btnEmployeesHire:ButtonBase;

    [SkinPart(required="true")]
    public var btnEmployeesTransfer:ButtonBase;

    public function set providerEmployees(value:IList):void {
        trace("!!! set providerEmployees");
        if (_providerEmployees != value) {
            _providerEmployees = value;
            _providerEmployeesChanged = true;

            invalidateProperties();
        }
    }

    protected function commitEmployees():void {
        if (_providerEmployeesChanged && (listEmployees || gridEmployees)) {
            _providerEmployeesChanged = false;

            if (listEmployees)
                listEmployees.dataProvider = _providerEmployees;

            if (gridEmployees)
                gridEmployees.dataProvider = _providerEmployees;
        }
    }

    override protected function commitProperties():void {
        super.commitProperties();
        commitEmployees();
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (listDepartments == instance) {
            listDepartments.dataProvider = _providerDepartments;
        } else if (listEmployees == instance) {
            listEmployees.dataProvider = _providerEmployees;
        } else if (gridEmployees == instance) {
            gridEmployees.dataProvider = _providerEmployees;
        } else if (listPositions == instance) {
            listPositions.dataProvider = _providerPositions;
        } else if (btnEmployeesRequest == instance) {
            btnEmployeesRequest.addEventListener(MouseEvent.CLICK, btnEmployeesRequest_clickHandler);
        } else if (btnEmployeesTransfer == instance) {
            btnEmployeesTransfer.addEventListener(MouseEvent.CLICK, btnEmployeesTransfer_clickHandler);
        } else if (btnEmployeesHire == instance) {
            btnEmployeesHire.addEventListener(MouseEvent.CLICK, btnEmployeesHire_clickHandler);
        } else if (btnDepartmentsAbolish == instance) {
            btnDepartmentsAbolish.addEventListener(MouseEvent.CLICK, btnDepartmentsAbolish_clickHandler);
        } else if (btnDepartmentsEstablish == instance) {
            btnDepartmentsEstablish.addEventListener(MouseEvent.CLICK, btnDepartmentsEstablish_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (btnEmployeesRequest == instance) {
            btnEmployeesRequest.removeEventListener(MouseEvent.CLICK, btnEmployeesRequest_clickHandler);
        } else if (btnEmployeesTransfer == instance) {
            btnEmployeesTransfer.removeEventListener(MouseEvent.CLICK, btnEmployeesTransfer_clickHandler);
        } else if (btnEmployeesHire == instance) {
            btnEmployeesHire.removeEventListener(MouseEvent.CLICK, btnEmployeesHire_clickHandler);
        } else if (btnDepartmentsAbolish == instance) {
            btnDepartmentsAbolish.removeEventListener(MouseEvent.CLICK, btnDepartmentsAbolish_clickHandler);
        } else if (btnDepartmentsEstablish == instance) {
            btnDepartmentsEstablish.removeEventListener(MouseEvent.CLICK, btnDepartmentsEstablish_clickHandler);
        }
    }

    public function setSkinEnabled():void {
        skin.enabled = true;
    }

    private function btnEmployeesRequest_clickHandler(event:MouseEvent):void {
        skin.enabled = false;
        dispatchEvent(new ShowDialogEvent(FilterView));
    }

    private function btnEmployeesTransfer_clickHandler(event:MouseEvent):void {
        skin.enabled = false;
        dispatchEvent(new ShowDialogEvent(TransferView));
    }

    private function btnEmployeesHire_clickHandler(event:MouseEvent):void {
        skin.enabled = false;
        dispatchEvent(new ShowDialogEvent(HireView));
    }

    private function btnDepartmentsAbolish_clickHandler(event:MouseEvent):void {
        skin.enabled = false;
        dispatchEvent(new ShowDialogEvent(AbolishView));
    }

    private function btnDepartmentsEstablish_clickHandler(event:MouseEvent):void {
        skin.enabled = false;
        dispatchEvent(new ShowDialogEvent(EstablishView));
    }
}
}
