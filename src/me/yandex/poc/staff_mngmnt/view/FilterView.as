/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 19.08.13
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.EmployeeFilterEvent;
import me.yandex.poc.staff_mngmnt.view.api.IFilterView;
import me.yandex.poc.staff_mngmnt.view.base.DialogView;

import spark.components.supportClasses.ButtonBase;
import spark.components.supportClasses.SkinnableTextBase;
import spark.components.supportClasses.ToggleButtonBase;

public class FilterView extends DialogView implements IFilterView {
    [SkinPart]
    public var btnFilter:ButtonBase;

    [SkinPart]
    public var textFirstName:SkinnableTextBase;

    [SkinPart]
    public var textLastName:SkinnableTextBase;

    [SkinPart]
    public var togglePositionsAny:ToggleButtonBase;

    [SkinPart]
    public var toggleFired:ToggleButtonBase;

    [SkinPart]
    public var togglePositionsInvert:ToggleButtonBase;

    [SkinPart]
    public var toggleDepartmentsAny:ToggleButtonBase;

    [SkinPart]
    public var toggleDepartmentsInvert:ToggleButtonBase;

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (btnFilter == instance) {
            btnFilter.addEventListener(MouseEvent.CLICK, btnFilter_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);

        if (btnFilter == instance) {
            btnFilter.removeEventListener(MouseEvent.CLICK, btnFilter_clickHandler);
        }
    }

    private function btnFilter_clickHandler(event:MouseEvent):void {
        dispatchEvent(new EmployeeFilterEvent(textFirstName.text, textLastName.text,
                togglePositionsAny.selected, togglePositionsInvert.selected,
                toggleDepartmentsAny.selected, toggleDepartmentsInvert.selected, toggleFired.selected));
    }
}
}
