/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.view {
import flash.events.MouseEvent;

import me.yandex.poc.staff_mngmnt.events.UserEvent;

import me.yandex.poc.staff_mngmnt.view.api.IWelcomeView;

import mx.managers.PopUpManager;

import spark.components.supportClasses.ButtonBase;

import spark.components.supportClasses.SkinnableComponent;

[SkinState("welcome")]
[SkinState("loading")]
[SkinState("pending")]

public class WelcomeView extends SkinnableComponent implements IWelcomeView {


    [SkinPart(required="false")]
    public var btnBrowse:ButtonBase;

    [SkinPart(required="false")]
    public var btnProceed:ButtonBase;

    public function hide():void {
        PopUpManager.removePopUp(this);
    }

    public function showLoadingState():void {
        skin.currentState = "loading";
    }

    public function showPendingState():void {
        skin.currentState = "pending";
    }

    override protected function partAdded(partName:String, instance:Object):void {
        super.partAdded(partName, instance);

        if (btnBrowse == instance) {
            btnBrowse.addEventListener(MouseEvent.CLICK, btnBrowse_clickHandler);
        } else if (btnProceed == instance) {
            btnProceed.addEventListener(MouseEvent.CLICK, btnProceed_clickHandler);
        }
    }

    override protected function partRemoved(partName:String, instance:Object):void {
        super.partRemoved(partName, instance);
        if (btnBrowse == instance) {
            btnBrowse.removeEventListener(MouseEvent.CLICK, btnBrowse_clickHandler);
        }
    }

    private function btnBrowse_clickHandler(event:MouseEvent):void {
        dispatchEvent(new UserEvent(UserEvent.BROWSE_FOR_DATABASE));
    }

    private function btnProceed_clickHandler(event:MouseEvent):void {
        dispatchEvent(new UserEvent(UserEvent.PROCEED_TO_APPLICATION));
    }
}
}
