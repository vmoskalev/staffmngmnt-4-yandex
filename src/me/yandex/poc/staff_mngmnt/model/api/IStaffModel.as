/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.model.api {
import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;

import mx.collections.IList;

public interface IStaffModel {
    function get listEmployees():IList;
    function get listDepartments():IList;
    function get listPositions():IList;

    function cleanUpAndPrepare():void;

    function get isInitialDataValid():Boolean;
    function get isDepartmentsListValid():Boolean;
    function get isPositionsListValid():Boolean;

    function get selectedDepartments():String;
    function get selectedPositions():String;
    function get selectedEmployees():String;

    function get vectorDepartmentsIDs():Vector.<int>;
    function get vectorPositionsIDs():Vector.<int>;

    function get isTotalEmployeesQuantityChanged():Boolean;
    function set isTotalEmployeesQuantityChanged(value:Boolean):void;

    function getPositionNameByID(nID:int):String;
    function getPositionID(sName:String):int;
    function getDepartmentNameByID(nID:int):String;

    function flushSelections():void;

    function appendDepartment(nID:int, sName:String):void;
    function flushDepartments():void;
    function validateDepartments():void;

    function appendPosition(nID:int, sName:String):void;
    function validatePositions():void;

    function updateEmployeesQuantityInDepartment(nID:int, value:int):void;
    function updateEmployeesQuantityInPosition(nID:int, value:int):void;

    function flushEmployeesList():void;
    function flushEmployeesDepartments(arrDeptIDs:Array):void;
    function transferEmployeesList(nDeptID:int):void;
    function appendEmployee(nID:int, sFirstName:String, sLastName:String, nDeptID:int, sPositionName:String):void;
    function appendFiredEmployee(nID:int, sFirstName:String, sLastName:String, sPositionName:String):void;
}
}
