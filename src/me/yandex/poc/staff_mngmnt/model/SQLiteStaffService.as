/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 12:58
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.model {
import flash.data.SQLConnection;
import flash.data.SQLMode;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLEvent;
import flash.filesystem.File;
import flash.utils.Dictionary;

import me.yandex.poc.staff_mngmnt.entities.Department;
import me.yandex.poc.staff_mngmnt.entities.Employee;
import me.yandex.poc.staff_mngmnt.entities.Position;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;

import me.yandex.poc.staff_mngmnt.entities.api.IEmployee;
import me.yandex.poc.staff_mngmnt.entities.api.IPosition;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;

import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import mx.collections.IList;

public class SQLiteStaffService {
    private var _connection:SQLConnection

    private var _listDepartments:Vector.<IDepartment>;
    private var _listPositions:Vector.<IPosition>;
    private var _listEmployees:Vector.<IEmployee>;

    private var _stashDepartments:Dictionary;
    private var _stashPositions:Dictionary;

    private var _departmentsCountPending:int = 0;
    private var _positionsCountPending:int = 0;

    private var _isDepartmentsQuantitiesValid:Boolean = false;
    private var _isDepartmentsListValid:Boolean = false;

    private var _isPositionsQuantitiesValid:Boolean = false;
    private var _isPositionsListValid:Boolean = false;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var dispatcher:IEventDispatcher;


    private function connection_openHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.connection_openHandler");

        _connection.addEventListener(SQLEvent.BEGIN, initialization_beginHandler);
        _connection.begin();
    }

    private function initialization_beginHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.initialization_beginHandler");
        _connection.addEventListener(SQLEvent.COMMIT, initialization_commitHandler);
        _connection.removeEventListener(SQLEvent.BEGIN, initialization_beginHandler);

        invalidateLists();
    }

    private function initialization_commitHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.initialization_commitHandler");
        _connection.removeEventListener(SQLEvent.COMMIT, initialization_commitHandler);
        _connection.addEventListener(SQLEvent.BEGIN, counting_beginHandler);
        _connection.begin();
    }

    private function counting_beginHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.counting_beginHandler");
        _connection.removeEventListener(SQLEvent.BEGIN, counting_beginHandler);
        _connection.addEventListener(SQLEvent.COMMIT, counting_commitHandler);

        invalidateQuantities();
    }

    private function counting_commitHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.counting_commitHandler");
        _connection.removeEventListener(SQLEvent.COMMIT, counting_commitHandler);

    }

    private function invalidateQuantities():void {
        _isDepartmentsQuantitiesValid = false;
        _isPositionsQuantitiesValid = false;

        departmentsQuantitiesUpdate();
        positionsQuantitiesUpdate();
    }

    private function invalidateLists():void {
        _isDepartmentsListValid = false;
        _isPositionsListValid = false;
    }

    private function commitQuantities():void {
        lumber.info("SQLiteStaffService.commitQuantities");
        if (_isDepartmentsQuantitiesValid && _isPositionsQuantitiesValid)
            _connection.commit();
    }

    private function commitLists():void {
        lumber.info("SQLiteStaffService.commitLists");
        if (_isDepartmentsListValid && _isPositionsListValid)
            _connection.commit();
    }

    /*
     * DEPARTMENTS BLOCK --BEGIN
     */

    private function departmentsQuantitiesUpdate():void {
        lumber.info("SQLiteStaffService.departmentsQuantitiesUpdate");
        _departmentsCountPending = 0;

        for each (var dept:IDepartment in _listDepartments) {
            _departmentsCountPending++;
            dept.nEmployeesQuantity = 0;
            var s:SQLStatement = new SQLStatement();
            s.sqlConnection = _connection;
            s.addEventListener(SQLEvent.RESULT, departmentCountResultHandler);
            s.text = "SELECT COUNT(*),DeptID FROM Employees WHERE DeptID=" + dept.nID;
            s.execute();
        }
    }

    private function departmentCountResultHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.departmentCountResultHandler");
        var s:SQLStatement = event.target as SQLStatement;
        var r:SQLResult = s.getResult();
        s.removeEventListener(SQLEvent.RESULT, departmentCountResultHandler);
        if (r.data[0].DeptID && _stashDepartments[r.data[0].DeptID] is IDepartment)
            (_stashDepartments[r.data[0].DeptID] as IDepartment).nEmployeesQuantity = r.data[0]["COUNT(*)"];

        if (--_departmentsCountPending > 0) return;

        _isDepartmentsQuantitiesValid = true;
        commitQuantities();
    }

    private function get departmentsListForSQLite():String {
        lumber.info("SQLiteStaffService.departmentsListForSQLite");
        var rslt:String = "";
        for each (var dptmnt:IDepartment in _listDepartments)
            rslt += dptmnt.selected ? (((rslt != "") ? "," : "") + dptmnt.nID) : "";
        return rslt;
    }

    /*
     * DEPARTMENTS BLOCK --END
     */


    /*
     * POSITIONS BLOCK --BEGIN
     */

    private function positionsRequest():void {
        lumber.info("SQLiteStaffService.requestPositions");


    }

    private function positionsQuantitiesUpdate():void {
        lumber.info("SQLiteStaffService.positionsQuantitiesUpdate");
        _positionsCountPending = 0;

        for each (var pos:IPosition in _listPositions) {
            _positionsCountPending++;
            pos.nEmployeesQuantity = 0;
            var s:SQLStatement = new SQLStatement();
            s.sqlConnection = _connection;
            s.addEventListener(SQLEvent.RESULT, positionCountResultHandler);
            s.text = 'SELECT COUNT(*) FROM Employees WHERE Position="' + pos.sName + '"';
            s.execute();
        }
    }


    private function positionCountResultHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.positionCountResultHandler");
        var s:SQLStatement = event.target as SQLStatement;
        var r:SQLResult = s.getResult();
        s.removeEventListener(SQLEvent.RESULT, positionCountResultHandler);
        if (r.data[0].Position && _stashPositions[r.data[0].Position] is IPosition)
            (_stashPositions[r.data[0].Position] as IPosition).nEmployeesQuantity = r.data[0]["COUNT(*)"];

        if (--_positionsCountPending > 0) return;

        _isPositionsQuantitiesValid = true;
        commitQuantities();
    }

    private function get positionsListForSQLite():String {
        lumber.info("SQLiteStaffService.positionsListForSQLite");
        var rslt:String = "";
        for each (var pstn:IPosition in _listPositions)
            rslt += pstn.selected ? (((rslt != "") ? ',"' : '"') + pstn.sName + '"') : "";
        return rslt;
    }

    /*
     * POSITIONS BLOCK --END
     */

    /*
     * EMPLOYEES BLOCK --BEGIN
     */

    public function employeesRequest():void {
        lumber.info("SQLiteStaffService.employeesRequest");
        employeesRequestPending();
    }

    private function employeesRequestPending(event:SQLEvent = null):void {
        lumber.info("SQLiteStaffService.employeesRequestPending");

        if (!event) _connection.addEventListener(SQLEvent.COMMIT, employeesRequestPending);
        if (_connection.inTransaction) return;

        _connection.removeEventListener(SQLEvent.COMMIT, employeesRequestPending);
        _connection.addEventListener(SQLEvent.BEGIN, employeesRequestBeginHandler);
        _connection.begin();
    }

    private function employeesRequestBeginHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesRequestBeginHandler");
        _connection.removeEventListener(SQLEvent.BEGIN, employeesRequestBeginHandler);

        var statement:SQLStatement = new SQLStatement();
        statement.addEventListener(SQLEvent.RESULT, employeesRequestResultHandler);
        statement.sqlConnection = _connection;
        statement.text = employeesRequestSQL;
        statement.execute();
    }

    private function employeesRequestResultHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesRequestResultHandler");

        var statement:SQLStatement = event.target as SQLStatement;
        statement.removeEventListener(SQLEvent.RESULT, employeesRequestResultHandler);
        _connection.commit();
        employeesInitialize(statement.getResult().data);
    }

    private function employeesInitialize(result:Array):void {
        lumber.info("SQLiteStaffService.employeesInitialize");

        _listEmployees = new <IEmployee>[];

        for each (var empl:Object in result) {
            var employee:IEmployee = new Employee(empl.EmplID, empl.FirstName, empl.LastName);
            employee.infoDepartment = _stashDepartments[empl.DeptID];
            employee.infoPosition = _stashPositions[empl.Position];
            employee.selected = false;
            _listEmployees.push(employee);
        }

        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_READY));
    }

    private function get employeesRequestSQL():String {
        lumber.info("SQLiteStaffService.employeesRequestSQL");

        return "SELECT * FROM Employees WHERE" +
                " DeptID IN (" + departmentsListForSQLite + ") AND" +
                " Position IN (" + positionsListForSQLite + ")";
    }

    private function get employeesListForSQLite():String {
        lumber.info("SQLiteStaffService.employeesListForSQLite");

        var rslt:String = "";
        for each (var empl:IEmployee in _listEmployees)
            rslt += ((rslt != "") ? ',' : '') + empl.nID;
        return rslt;
    }

    private var _tmpdept:IDepartment;

    public function employeesTransfer(department:IDepartment = null, employees:IList = null):void {
        lumber.info("SQLiteStaffService.employeesTransfer");

        _tmpdept = department;
        employeesTransferPending();
    }

    private function employeesTransferPending(event:SQLEvent = null):void {
        lumber.info("SQLiteStaffService.employeesTransferPending");

        if (!event)    _connection.addEventListener(SQLEvent.COMMIT, employeesTransferPending);
        if (_connection.inTransaction) return;

        employeesTransferImpl();
    }

    private function employeesTransferImpl():void {
        lumber.info("SQLiteStaffService.employeesTransferImpl");

        _connection.removeEventListener(SQLEvent.COMMIT, employeesTransferPending);
        _connection.addEventListener(SQLEvent.BEGIN, employeesTransferBeginHandler);
        _connection.begin();
    }

    private function employeesTransferBeginHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesTransferBeginHandler");
        _connection.removeEventListener(SQLEvent.BEGIN, employeesTransferBeginHandler);
        _connection.addEventListener(SQLEvent.COMMIT, employeesTransferCommitHandler);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_TRANSFER_BEGIN));

        employeesTransferRequest(_tmpdept);
    }

    private function employeesTransferCommitHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesTransferCommitHandler");
        _connection.removeEventListener(SQLEvent.COMMIT, employeesTransferCommitHandler);

        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_TRANSFER_COMMIT));
        employeesTransferAfterpartyPending();
    }

    private function employeesTransferAfterpartyPending(event:SQLEvent = null):void {
        lumber.info("SQLiteStaffService.employeesTransferAfterpartyPending");
        if (!event) _connection.addEventListener(SQLEvent.COMMIT, employeesTransferAfterpartyPending);
        if (_connection.inTransaction) return;

        _connection.removeEventListener(SQLEvent.COMMIT, employeesTransferAfterpartyPending);
        _connection.addEventListener(SQLEvent.BEGIN, employeesTransferAfterpartyImpl);
        _connection.begin();
    }

    private function employeesTransferAfterpartyImpl(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesTransferAfterpartyImpl");
        _connection.removeEventListener(SQLEvent.BEGIN, employeesTransferAfterpartyImpl);
        _connection.addEventListener(SQLEvent.COMMIT, employeesTransferAfterpartyCommitHandler);

        invalidateQuantities();
    }

    private function employeesTransferAfterpartyCommitHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesTransferAfterpartyCommitHandler");
        _connection.removeEventListener(SQLEvent.COMMIT, employeesTransferAfterpartyCommitHandler);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_TRANSFER_COMPLETE));
    }

    private function employeesTransferRequest(department:IDepartment):void {
        lumber.info("SQLiteStaffService.employeesTransferImpl");
        var s:SQLStatement = new SQLStatement();
        s.addEventListener(SQLEvent.RESULT, employeesTransferImpl_resultHandler);
        s.sqlConnection = _connection;
        s.text = department == null ? "DELETE FROM Employees WHERE EmplID IN (" + employeesListForSQLite + ")"
                : "UPDATE Employees SET DeptID=" + department.nID + " WHERE EmplID IN (" + employeesListForSQLite + ")";
        s.execute();
    }

    private function employeesTransferImpl_resultHandler(event:SQLEvent):void {
        lumber.info("SQLiteStaffService.employeesTransferImpl_resultHandler");
        _connection.commit();

        _listEmployees = new <IEmployee>[];
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_READY));
    }

    public function employeesHire(sFirstName:String, sLastName:String):void {
    }

    /*
     * EMPLOYEES BLOCK --END
     */


    /*
     * IStaffModel implementation
     */


}
}
