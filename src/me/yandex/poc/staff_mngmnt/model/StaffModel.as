/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 9:35
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.model {
import flash.utils.Dictionary;

import me.yandex.poc.staff_mngmnt.entities.Department;
import me.yandex.poc.staff_mngmnt.entities.Employee;

import me.yandex.poc.staff_mngmnt.entities.Position;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;
import me.yandex.poc.staff_mngmnt.entities.api.IEmployee;
import me.yandex.poc.staff_mngmnt.entities.api.IPosition;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import mx.collections.ArrayCollection;

import mx.collections.IList;

import org.apache.flex.collections.VectorList;

import org.apache.flex.collections.VectorList;

public class StaffModel implements IStaffModel {

    private var _listDepartments:Vector.<IDepartment>;
    private var _listPositions:Vector.<IPosition>;
    private var _listEmployees:Vector.<IEmployee>;

    private var _stashDepartments:Dictionary;
    private var _stashPositions:Dictionary;

    private var _isDepartmentsQuantitiesValid:Boolean = false;
    private var _isDepartmentsListValid:Boolean = false;

    private var _isPositionsQuantitiesValid:Boolean = false;
    private var _isPositionsListValid:Boolean = false;

    private var _isTotalEmployeesQuantityChanged:Boolean = true;

    private var _noDeptID:IDepartment;

    [Inject]
    public var lumber:ILumber;


    public function get isDepartmentsListValid():Boolean {
        return _isDepartmentsListValid;
    }

    public function get isPositionsListValid():Boolean {
        return _isPositionsListValid;
    }

    public function get isInitialDataValid():Boolean {
        return isDepartmentsListValid && isPositionsListValid;
    }

    public function cleanUpAndPrepare():void {
        lumber.info("SQLiteStaffService.clean");

        _noDeptID = new Department(0, "УВОЛЕН", 0);

        _listPositions = new <IPosition>[];
        _stashPositions = new Dictionary();

        flushDepartments();
        flushEmployeesList();

        _isPositionsListValid = false;
        _isPositionsQuantitiesValid = false;
    }


    public function getPositionNameByID(nID:int):String {
        return _stashPositions[nID] is IPosition ? _stashPositions[nID].sName : "positions stash fail";
    }

    public function getPositionID(sName:String):int {
        return _stashPositions[sName] is IPosition ? _stashPositions[sName].nID : 1;
    }

    public function getDepartmentNameByID(nID:int):String {
        return _stashDepartments[nID] is IDepartment ? _stashDepartments[nID].sName : "departments stash fail";
    }

    public function get selectedDepartments():String {
        var ids:Array = [];
        var len:uint = _listDepartments.length;
        for (var i:uint = 0; i < len; i++)
            if (_listDepartments[i].selected)
                ids.push(_listDepartments[i].nID);
        return ids.toString();
    }

    public function get selectedPositions():String {
        var ids:Array = [];
        var len:uint = _listPositions.length;
        for (var i:uint = 0; i < len; i++)
            if (_listPositions[i].selected)
                ids.push("'" + _listPositions[i].sName + "'");
        return ids.toString();
    }

    public function get selectedEmployees():String {
        return _listEmployees.toString();
    }


    public function get vectorDepartmentsIDs():Vector.<int> {
        var ids:Vector.<int> = new <int>[];
        var len:int = _listDepartments.length;
        for (var i:int = 0; i < len; i++)
            ids.push(_listDepartments[i].nID);

        lumber.info(ids);
        return ids;
    }

    public function get vectorPositionsIDs():Vector.<int> {
        var ids:Vector.<int> = new <int>[];
        var len:int = _listPositions.length;
        for (var i:int = 0; i < len; i++)
            ids.push(_listPositions[i].nID);

        lumber.info(ids);
        return ids;
    }


    public function get isTotalEmployeesQuantityChanged():Boolean {
        return _isTotalEmployeesQuantityChanged;
    }

    public function set isTotalEmployeesQuantityChanged(value:Boolean):void {
        _isTotalEmployeesQuantityChanged = value;
    }

    public function flushSelections():void {
        var len:uint = _listDepartments.length;
        for (var i:uint = 0; i < len; i++)
            _listDepartments[i].selected = false;

        len = _listPositions.length;
        for (i = 0; i < len; i++)
            _listPositions[i].selected = false;
    }

    public function updateEmployeesQuantityInDepartment(nID:int, value:int):void {
        var dept:IDepartment = _stashDepartments[nID] as IDepartment;
        if (!dept) {
            lumber.fail("StaffModel.updateEmployeesQuantityInDepartment » no department");
            return;
        }

        dept.nEmployeesQuantity = value;
    }

    public function updateEmployeesQuantityInPosition(nID:int, value:int):void {
        var pos:IPosition = _stashPositions[nID] as IPosition;
        if (!pos) return;

        pos.nEmployeesQuantity = value;
    }

    public function flushDepartments():void {
        _listDepartments = new <IDepartment>[];
        _stashDepartments = new Dictionary();

        _isDepartmentsListValid = false;
        _isDepartmentsQuantitiesValid = false;
    }

    public function appendDepartment(nID:int, sName:String):void {
        _stashDepartments[nID] = new Department(nID, sName);
        _listDepartments.push(_stashDepartments[nID] as IDepartment);
    }

    public function validateDepartments():void {
        _isDepartmentsListValid = true;
    }

    public function appendPosition(nID:int, sName:String):void {
        _stashPositions[nID] = _stashPositions[sName] = new Position(nID, sName);
        _listPositions.push(_stashPositions[nID] as IPosition);
    }

    public function validatePositions():void {
        _isPositionsListValid = true;
    }

    public function flushEmployeesList():void {
        _listEmployees = new <IEmployee>[];
    }

    public function flushEmployeesDepartments(arrDeptIDs:Array):void {
        var len:int = _listEmployees.length;
        var ac:ArrayCollection = new ArrayCollection(arrDeptIDs);
        for (var i:int = 0; i < len; i++)
            if (ac.contains(_listEmployees[i].infoDepartment.nID.toString()))
                _listEmployees[i].infoDepartment = _noDeptID;
    }

    public function transferEmployeesList(nDeptID:int):void {
        if (nDeptID < 0) {
            isTotalEmployeesQuantityChanged = true;
            flushEmployeesList();
            return;
        }

        var dept:IDepartment = _stashDepartments[nDeptID] as IDepartment;
        if (!dept && nDeptID == 0) dept = _noDeptID;
        if (!dept) {
            flushEmployeesList();
            return;
        }

        var len:int = _listEmployees.length;
        for (var i:int = 0; i < len; i++)
            _listEmployees[i].infoDepartment = dept;
    }

    public function appendEmployee(nID:int, sFirstName:String, sLastName:String, nDeptID:int, sPositionName:String):void {
        var empl:IEmployee = new Employee(nID, sFirstName, sLastName);
        empl.infoDepartment = _stashDepartments[nDeptID] as IDepartment;
        empl.infoPosition = _stashPositions[sPositionName] as IPosition;

        if (!empl.infoPosition || !empl.infoDepartment) {
//            lumber.fail(sFirstName + "\t" + sLastName + "  ",nDeptID, sPositionName);
            return;
        }

//        lumber.info(sFirstName + "\t" + sLastName + "  ",nDeptID, sPositionName);

        _listEmployees.push(empl);
    }

    public function appendFiredEmployee(nID:int, sFirstName:String, sLastName:String, sPositionName:String):void {
        var empl:IEmployee = new Employee(nID, sFirstName, sLastName);
        empl.infoPosition = _stashPositions[sPositionName] as IPosition;
        empl.infoDepartment = _noDeptID;

        if (!empl.infoPosition) {
//            lumber.fail(sFirstName + "\t" + sLastName + "  ",nDeptID, sPositionName);
            return;
        }

//        lumber.info(sFirstName + "\t" + sLastName + "  ",nDeptID, sPositionName);

        _listEmployees.push(empl);
    }

    public function get listEmployees():IList {
        lumber.info();
        return new VectorList(_listEmployees);
    }

    public function get listDepartments():IList {
        lumber.info();
        return new VectorList(_listDepartments);
    }

    public function get listPositions():IList {
        lumber.info();
        return new VectorList(_listPositions);
    }
}
}
