/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands {
import flash.events.IEventDispatcher;

import me.yandex.poc.staff_mngmnt.services.api.ISQLiteWorkerService;

import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import robotlegs.bender.bundles.mvcs.Command;

public class StaffManagerClosing extends Command {
    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var service:ISQLiteWorkerService;

    [Inject]
    public var dispatcher:IEventDispatcher;

    override public function execute():void {
        lumber.info();
        service.close();
    }
}
}
