/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands {
import flash.display.DisplayObjectContainer;

import me.yandex.poc.staff_mngmnt.events.ShowDialogEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;
import me.yandex.poc.staff_mngmnt.view.api.base.IDialogView;

import mx.managers.PopUpManager;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.viewManager.api.IViewManager;
import robotlegs.bender.framework.api.IInjector;

public class ShowDialog extends Command {
    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var cv:ContextView;

    [Inject]
    public var injector:IInjector;

    [Inject]
    public var viewManager:IViewManager;

    [Inject]
    public var event:ShowDialogEvent;

    [Inject]
    public var model:IStaffModel;

    override public function execute():void {
        lumber.info();
        model.flushSelections();
        var dialog:IDialogView = injector.getOrCreateNewInstance(event.dialogClass);
        viewManager.addContainer(dialog as DisplayObjectContainer);
        PopUpManager.addPopUp(dialog, cv.view);
        PopUpManager.centerPopUp(dialog);
    }
}
}
