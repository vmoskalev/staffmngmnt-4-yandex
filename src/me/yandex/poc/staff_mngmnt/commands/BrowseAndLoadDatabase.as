/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 14:07
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands {
import flash.data.SQLConnection;
import flash.data.SQLMode;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.SQLEvent;
import flash.filesystem.File;
import flash.net.FileFilter;

import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.services.api.ISQLiteWorkerService;

import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IInjector;

public class BrowseAndLoadDatabase extends Command {
    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var injector:IInjector;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var sqlite:ISQLiteWorkerService;

    override public function execute():void {
        lumber.info();


        var dbfile:File = File.desktopDirectory;
        dbfile.addEventListener(Event.SELECT, dbfile_selectHandler);
        dbfile.browseForOpen("Select DB", [new FileFilter("Database", "*.db")]);
    }

    private function dbfile_selectHandler(event:Event):void {
        lumber.info();

        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.CONNECTION_PREPARING));
        sqlite.initialize((event.target as File));

//        (event.target as File).removeEventListener(Event.SELECT, dbfile_selectHandler);
//        var connection:SQLConnection = new SQLConnection();
//        connection.addEventListener(SQLEvent.OPEN, connection_openHandler);
//        connection.openAsync(event.target as File, SQLMode.UPDATE);
    }

    private function connection_openHandler(event:SQLEvent):void {
        lumber.info();

        (event.target as SQLConnection).removeEventListener(SQLEvent.OPEN, connection_openHandler);
        injector.map(SQLConnection).toValue(event.target);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.CONNECTION_ESTABLISHED));
    }
}
}
