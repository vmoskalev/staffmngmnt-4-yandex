/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 21.08.13
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands {
import flash.events.IEventDispatcher;

import me.yandex.poc.staff_mngmnt.events.UpdateQuantityEvent;

import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;

import robotlegs.bender.bundles.mvcs.Command;

public class UpdateQuantities extends Command{
    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var model:IStaffModel;

    override public function execute():void {
        var list:Vector.<int> = model.vectorDepartmentsIDs;
        var len:int = list.length;
        var i:int;

        for (i = 0; i < len; i++)
            dispatcher.dispatchEvent(new UpdateQuantityEvent(UpdateQuantityEvent.UPDATE_QUANTITY_IN_DEPARTMENT, list[i]));

        if (model.isTotalEmployeesQuantityChanged) {
            model.isTotalEmployeesQuantityChanged = false;

            list = model.vectorPositionsIDs;
            len = list.length;

            for (i = 0; i < len; i++)
                dispatcher.dispatchEvent(new UpdateQuantityEvent(UpdateQuantityEvent.UPDATE_QUANTITY_IN_POSITION, list[i]));
        }
    }
}
}
