/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 10.08.13
 * Time: 9:37
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {

import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;
import me.yandex.poc.staff_mngmnt.events.EmployeeFilterEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;

public class EmployeesRequest extends SQLServiceCommand implements ISQLServiceCommandInheritor {

    [Inject]
    public var event:EmployeeFilterEvent;

    private var _request:Array;
    private var expName:RegExp = /[ЙЦУКЕФВАПЯЧСМИНГШЗХРОЛДЖЭТБЮ][а-я]*/;

    public function get request():Array {
        var strRequest:String = "SELECT * FROM Employees";
        var conditions:Vector.<String> = new <String>[];

        if (isFirstNameValid)
            conditions.push("FirstName LIKE '%" + event.firstName + "%'");

        if (isLastNameValid)
            conditions.push("LastName LIKE '%" + event.lastName + "%'");

        if (!event.isDepartmentsAny) {
            if (event.isFired)
                conditions.push("DeptID=0");
            else
                conditions.push("DeptID" + (event.isDepartmentsInvert ? " NOT" : "") + " IN (" + model.selectedDepartments + ")");
        }

        if (!event.isPositionsAny)
            conditions.push("Position" + (event.isPositionsInvert ? " NOT" : "") + " IN (" + model.selectedPositions + ")");

        var len:uint = conditions.length;
        if (len > 0) strRequest += " WHERE ";

        for (var i:uint = 0; i < len; i++) {
            if (i > 0) strRequest += " AND ";
            strRequest += conditions[i];
        }

        lumber.info(strRequest);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_REQUEST_BEGIN));
        _request = [strRequest];
        return _request;
    }

    public function applyResult(result:Array):void {
        lumber.info();
        model.flushEmployeesList();
        for each (var obj:Object in result) {
//            if (obj.DeptID <= 0) {
//                lumber.fail(JSON.stringify(obj));
//                continue;
//            }
            if (event.isFired && !event.isDepartmentsAny)
                model.appendFiredEmployee(obj.EmplID, obj.FirstName, obj.LastName, obj.Position);
            else
                model.appendEmployee(obj.EmplID, obj.FirstName, obj.LastName, obj.DeptID, obj.Position);
        }

        lumber.info(result.length);

        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_REQUEST_COMPLETE));
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_READY));
    }

    private function get isFirstNameValid():Boolean {
        var isRegExpPass:Boolean = expName.test(event.firstName);
        return event.firstName && event.firstName != "" && isRegExpPass;
    }

    private function get isLastNameValid():Boolean {
        var isRegExpPass:Boolean = expName.test(event.lastName);
        return event.lastName && event.lastName != "" && isRegExpPass;
    }
}
}
