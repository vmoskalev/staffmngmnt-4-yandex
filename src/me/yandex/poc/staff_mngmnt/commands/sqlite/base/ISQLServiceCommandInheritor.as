/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 21.08.13
 * Time: 8:22
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite.base {
public interface ISQLServiceCommandInheritor {
    function get request():Array;
    function applyResult(result:Array):void;
}
}
