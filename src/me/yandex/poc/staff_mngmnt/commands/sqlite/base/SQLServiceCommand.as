/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 21.08.13
 * Time: 8:03
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite.base {

import avmplus.getQualifiedClassName;

import flash.events.IEventDispatcher;

import me.yandex.poc.staff_mngmnt.events.SQLServiceResultEvent;
import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.services.api.ISQLiteWorkerService;
import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import robotlegs.bender.bundles.mvcs.Command;

public class SQLServiceCommand extends Command {
    [Inject]
    public var model:IStaffModel;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var service:ISQLiteWorkerService;

    [Inject]
    public var lumber:ILumber;

    protected var _inheritor:ISQLServiceCommandInheritor = this as ISQLServiceCommandInheritor;

    override public function execute():void {
        if (!_inheritor) throw (new Error("bad inheritance"));
        lumber.info(getQualifiedClassName(this).split("::")[1]);
        service.addEventListener(service.execute(_inheritor.request), resultHandler);
    }

    final private function resultHandler(event:SQLServiceResultEvent):void {
        lumber.info(getQualifiedClassName(this).split("::")[1], event.type);
        service.removeEventListener(event.type, resultHandler);
        _inheritor.applyResult(event.result);
    }
}
}
