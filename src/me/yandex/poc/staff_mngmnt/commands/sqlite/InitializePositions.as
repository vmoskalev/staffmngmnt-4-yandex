/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 9:25
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;

import me.yandex.poc.staff_mngmnt.events.InternalEvent;

public class InitializePositions extends SQLServiceCommand implements ISQLServiceCommandInheritor{
    public function get request():Array {
        lumber.info();
        return ['SELECT DISTINCT Position FROM Employees'];
    }

    public function applyResult(result:Array):void {
        lumber.info();

        var len:int = result.length;

        for (var i:uint = 0; i < len; i++)
            model.appendPosition(i + 1, result[i].Position);

        model.validatePositions();
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.POSITIONS_READY));

        if (model.isInitialDataValid)
            dispatcher.dispatchEvent(new InternalEvent(InternalEvent.ENVIRONMENT_READY));
    }
}
}
