/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;
import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;

public class DepartmentEstablish extends SQLServiceCommand implements ISQLServiceCommandInheritor {
    [Inject]
    public var event:DepartmentEvent;

    private var expName:RegExp = /[ЙЦУКЕФВАПЯЧСМИНГШЗХРОЛДЖЭТБЮ№0-9][А-Яа-я0-9 №:]*/;

    public function get request():Array {
        if (event.deptName && event.deptName != "" && !expName.test(event.deptName))
            throw new Error("wrong department name");
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENT_ESTABLISH_BEGIN));
        return ["INSERT INTO Departments (DeptName) VALUES ('" + event.deptName + "')"];
    }

    public function applyResult(result:Array):void {
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENT_ESTABLISH_COMPLETE));
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENTS_UPDATE));
    }
}
}
