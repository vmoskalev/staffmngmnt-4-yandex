/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;
import me.yandex.poc.staff_mngmnt.events.DepartmentEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;

public class DepartmentAbolish extends SQLServiceCommand implements ISQLServiceCommandInheritor {
    [Inject]
    public var event:DepartmentEvent;

    private var _dpts:Array;

    public function get request():Array {
        _dpts = model.selectedDepartments.split(",");
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENT_ABOLISH_BEGIN));
        return ["UPDATE Employees SET DeptID=0 WHERE DeptID IN (" + model.selectedDepartments + ")",
                "DELETE FROM Departments WHERE DeptID IN (" + model.selectedDepartments + ")"];
    }

    public function applyResult(result:Array):void {
        model.flushEmployeesDepartments(_dpts);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENT_ABOLISH_COMPLETE));
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENTS_UPDATE));
    }
}
}
