/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 9:08
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;

import me.yandex.poc.staff_mngmnt.events.InternalEvent;

public class InitializeDepartments extends SQLServiceCommand implements ISQLServiceCommandInheritor{


    public function get request():Array {
        return ['SELECT * FROM Departments'];
    }

    public function applyResult(result:Array):void {
        lumber.info();

        model.flushDepartments();

        for each (var dept:Object in result)
            model.appendDepartment(dept.DeptID, dept.DeptName);

        model.validateDepartments();
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.DEPARTMENTS_READY));

        if (model.isInitialDataValid)
            dispatcher.dispatchEvent(new InternalEvent(InternalEvent.ENVIRONMENT_READY));
    }
}
}
