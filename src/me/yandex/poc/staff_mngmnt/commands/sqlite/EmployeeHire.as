/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 8:14
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;
import me.yandex.poc.staff_mngmnt.events.EmployeeHireEvent;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.UpdateQuantityEvent;

public class EmployeeHire extends SQLServiceCommand implements ISQLServiceCommandInheritor {
    [Inject]
    public var event:EmployeeHireEvent;

    private var expName:RegExp = /[ЙЦУКЕФВАПЯЧСМИНГШЗХРОЛДЖЭТБЮ][а-я]*/;

    public function get request():Array {
        if (!isFirstNameValid || !isLastNameValid || event.department < 0 || event.position == "''")
            throw new Error("invalid hire arguments");

        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_HIRING_BEGIN));
        return ["INSERT INTO Employees (DeptID, FirstName, LastName, Position) VALUES (" + [event.department, firstName, lastName, position] + ")"];
    }

    public function applyResult(result:Array):void {
        dispatcher.dispatchEvent(new UpdateQuantityEvent(UpdateQuantityEvent.UPDATE_QUANTITY_IN_DEPARTMENT, event.department));
        dispatcher.dispatchEvent(new UpdateQuantityEvent(UpdateQuantityEvent.UPDATE_QUANTITY_IN_POSITION, model.getPositionID(event.position)));
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_HIRING_COMPLETE));
    }

    private function get isFirstNameValid():Boolean {
        var isRegExpPass:Boolean = expName.test(event.firstName);
        return event.firstName && event.firstName != "" && isRegExpPass;
    }

    private function get isLastNameValid():Boolean {
        var isRegExpPass:Boolean = expName.test(event.lastName);
        return event.lastName && event.lastName != "" && isRegExpPass;
    }

    private function get firstName():String {
        return "'" + event.firstName + "'";
    }

    private function get lastName():String {
        return "'" + event.lastName + "'";
    }

    private function get position():String {
        return "'" + event.position + "'";
    }
}
}
