/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 18:03
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;
import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.TransferEvent;

public class EmployeesTransfer extends SQLServiceCommand implements ISQLServiceCommandInheritor {
    [Inject]
    public var event:TransferEvent;

    private var _request:Array;

    public function get request():Array {
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_TRANSFER_BEGIN));
        _request = ["UPDATE Employees SET DeptID=" + event.nDeptID
                + " WHERE EmplID IN (" + model.selectedEmployees + ")"];
        return _request;
    }

    public function applyResult(result:Array):void {
        lumber.info();
        model.transferEmployeesList(event.nDeptID);
        dispatcher.dispatchEvent(new InternalEvent(InternalEvent.EMPLOYEES_TRANSFER_COMPLETE));
    }
}
}
