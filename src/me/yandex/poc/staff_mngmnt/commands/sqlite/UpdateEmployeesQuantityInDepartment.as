/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.commands.sqlite {
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.ISQLServiceCommandInheritor;
import me.yandex.poc.staff_mngmnt.commands.sqlite.base.SQLServiceCommand;

import me.yandex.poc.staff_mngmnt.events.UpdateQuantityEvent;

public class UpdateEmployeesQuantityInDepartment extends SQLServiceCommand implements ISQLServiceCommandInheritor {
    [Inject]
    public var event:UpdateQuantityEvent;

    public function get request():Array {
        return ["SELECT COUNT(*) FROM Employees WHERE DeptID=" + event.nID];
    }

    public function applyResult(result:Array):void {
        lumber.info(event.nID, model.getDepartmentNameByID(event.nID), result.length > 0 ? result[0]["COUNT(*)"] : 0);
        model.updateEmployeesQuantityInDepartment(this.event.nID, result.length > 0 ? result[0]["COUNT(*)"] : 0);
    }
}
}
