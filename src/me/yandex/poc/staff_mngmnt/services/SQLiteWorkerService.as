/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 12:21
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.services {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import flash.filesystem.File;
import flash.system.MessageChannel;
import flash.system.Worker;
import flash.system.WorkerDomain;
import flash.utils.ByteArray;

import me.yandex.poc.staff_mngmnt.events.InternalEvent;
import me.yandex.poc.staff_mngmnt.events.SQLServiceResultEvent;

import me.yandex.poc.staff_mngmnt.model.api.IStaffModel;
import me.yandex.poc.staff_mngmnt.services.api.ISQLiteWorkerService;

import me.yandex.poc.staff_mngmnt.utils.api.ILumber;

import mx.utils.UIDUtil;

public class SQLiteWorkerService extends EventDispatcher implements ISQLiteWorkerService {
    [Inject]
    public var model:IStaffModel;

    [Inject]
    public var lumber:ILumber;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Embed(source="../../../../../../workers/SQLiteWorker.swf", mimeType="application/octet-stream")]
    public var WorkerSWF:Class;

    private var _worker:Worker;
    private var _worker2service:MessageChannel;
    private var _service2worker:MessageChannel;

    private var _isInitialized:Boolean = false;
    private var _gateway:ByteArray;

    public function initialize(file:File):void {
        if (_isInitialized) return;

        lumber.info();
        _worker = WorkerDomain.current.createWorker(ByteArray(new WorkerSWF), true);

        _worker2service = _worker.createMessageChannel(Worker.current);
        _service2worker = Worker.current.createMessageChannel(_worker);

        _worker.setSharedProperty("w2s", _worker2service);
        _worker.setSharedProperty("s2w", _service2worker);

        _worker.setSharedProperty("startTime", lumber.startTime.toString());


        _worker.start();
        _gateway = _worker2service.receive(true) as ByteArray;

        lumber.info("gateway");
        _worker.setSharedProperty("filePath", file.nativePath);
        _service2worker.send(JSON.stringify({type:"FILE", path:file.nativePath}));

        _worker2service.addEventListener(Event.CHANNEL_MESSAGE, responseHandler);
        _isInitialized = true;
    }

    public function close():void {
        lumber.info();
        _service2worker.send(JSON.stringify({type:"CLOSE"}));
    }

    public function execute(request:Array):String {
        lumber.info(request);
        if (!request) throw new Error("null request");
        if (request.length == 0) throw new Error("empty request");

        var uid:String = UIDUtil.createUID();
        var type:String = request.length > 1 ? "TRANSACTION" : "REQUEST"
        _service2worker.send(JSON.stringify({uid:uid, request:request, type:type}));

        return uid;
    }

    private function responseHandler(event:Event):void {
//        lumber.info("SQLiteWorkerService.responseHandler");

        var response:String = _worker2service.receive();
        if (!response) {
//            _gateway = _worker2service.receive() as ByteArray;
            lumber.info("gateway");
            return;
        }

        var resp:Object = JSON.parse(response);
        lumber.info(response);

        switch (resp.info) {
            case "connection_closed":
                dispatcher.dispatchEvent(new InternalEvent(InternalEvent.CONNECTION_CLOSED));
                break;
            case "connection_established":
                dispatcher.dispatchEvent(new InternalEvent(InternalEvent.CONNECTION_ESTABLISHED));
                break;
            case "commit":
                dispatchEvent(new SQLServiceResultEvent(resp.type, []));
            case "result":
                _gateway.position = 0;
                dispatchEvent(new SQLServiceResultEvent(resp.type, _gateway.readObject() as Array, resp.token));
                break;
        }

    }
}
}
