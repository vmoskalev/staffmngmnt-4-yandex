/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.services.api {
import flash.events.IEventDispatcher;
import flash.filesystem.File;

public interface ISQLiteWorkerService extends IEventDispatcher{
    function initialize(file:File):void;
    function execute(request:Array):String;
    function close():void;
}
}
