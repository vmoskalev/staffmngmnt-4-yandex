/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 10:07
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class DepartmentEvent extends Event {
    public static const ESTABLISH:String = "de_establish_department";
    public static const ABOLISH:String = "de_abolish_department";

    private var _deptName:String;

    public function DepartmentEvent(type:String, deptName:String = "") {
        super(type, false, false);
        _deptName = deptName;
    }

    public function get deptName():String {
        return _deptName;
    }

    override public function clone():Event {
        return new DepartmentEvent(type, deptName);
    }
}
}
