/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 19.08.13
 * Time: 20:27
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class EmployeeFilterEvent extends Event {
    public static const FILTER_EMPLOYEES:String = "filter_employees";

    private var _isPositionsAny:Boolean;
    private var _isPositionsInvert:Boolean;

    private var _isDepartmentsAny:Boolean;
    private var _isDepartmentsInvert:Boolean;

    private var _isFired:Boolean;

    private var _firstName:String;
    private var _lastName:String;


    public function get isPositionsAny():Boolean {
        return _isPositionsAny;
    }

    public function get isPositionsInvert():Boolean {
        return _isPositionsInvert;
    }

    public function get isDepartmentsAny():Boolean {
        return _isDepartmentsAny;
    }

    public function get isDepartmentsInvert():Boolean {
        return _isDepartmentsInvert;
    }

    public function get isFired():Boolean {
        return _isFired;
    }

    public function get firstName():String {
        return _firstName;
    }

    public function get lastName():String {
        return _lastName;
    }

    public function EmployeeFilterEvent(firstName:String, lastName:String, isPosAny:Boolean, isPosInv:Boolean, isDeptAny:Boolean, isDeptInv:Boolean, isFired:Boolean) {
        super(FILTER_EMPLOYEES, false, false);

        _isDepartmentsAny = isDeptAny;
        _isDepartmentsInvert = isDeptInv;
        _isPositionsAny = isPosAny;
        _isPositionsInvert = isPosInv;

        _isFired = isFired;

        _firstName = firstName;
        _lastName = lastName;
    }

    override public function clone():Event {
        return new EmployeeFilterEvent(firstName, lastName, isPositionsAny, isPositionsInvert, isDepartmentsAny, isDepartmentsInvert, isFired);
    }
}
}
