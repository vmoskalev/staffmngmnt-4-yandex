/**
 * Created with IntelliJ IDEA.
 * User: vmoskalev
 * Date: 07.08.13
 * Time: 10:49
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class InternalEvent extends Event {
    public static const CONNECTION_PREPARING:String = "internal_sqlite_connection_preparing";
    public static const CONNECTION_ESTABLISHED:String = "internal_sqlite_connection_established";
    public static const CONNECTION_CLOSED:String = "internal_sqlite_connection_closed";

    public static const DEPARTMENTS_READY:String = "internal_departments_ready";
    public static const POSITIONS_READY:String = "internal_positions_ready";
    public static const EMPLOYEES_READY:String = "internal_employees_ready";

    public static const EMPLOYEES_HIRING_BEGIN:String = "internal_employees_hiring_begin";
    public static const EMPLOYEES_HIRING_COMPLETE:String = "internal_employees_hiring_complete";

    public static const DEPARTMENTS_UPDATE:String = "internal_department_update";

    public static const DEPARTMENT_ESTABLISH_BEGIN:String = "internal_department_establish_begin";
    public static const DEPARTMENT_ESTABLISH_COMPLETE:String = "internal_department_establish_complete";

    public static const DEPARTMENT_ABOLISH_BEGIN:String = "internal_department_abolish_begin";
    public static const DEPARTMENT_ABOLISH_COMPLETE:String = "internal_department_abolish_complete";

    public static const EMPLOYEES_TRANSFER_BEGIN:String = "internal_employees_transfer_begin";
    public static const EMPLOYEES_TRANSFER_COMMIT:String = "internal_employees_transfer_commit";
    public static const EMPLOYEES_TRANSFER_COMPLETE:String = "internal_employees_transfer_complete";
    public static const EMPLOYEES_TRANSFER_REJECT:String = "internal_employees_transfer_reject";

    public static const EMPLOYEES_REQUEST_BEGIN:String = "internal_employees_request_begin";
    public static const EMPLOYEES_REQUEST_COMPLETE:String = "internal_employees_request_complete";
    public static const EMPLOYEES_REQUEST_FAULT:String = "internal_employees_request_fault";

    public static const ENVIRONMENT_READY:String = "internal_db_environment_ready";
    public static const HIDE_DIALOG:String = "internal_hide_dialog";

    public function InternalEvent(type:String) {
        super(type, false, false);
    }

    override public function clone():Event {
        return new InternalEvent(type);
    }
}
}
