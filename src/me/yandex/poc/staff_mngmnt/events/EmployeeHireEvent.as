/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 12.08.13
 * Time: 21:27
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class EmployeeHireEvent extends Event {
    public static const HIRE:String = "hire_new_employee";

    private var _lastName:String;
    private var _firstName:String;
    private var _position:String;
    private var _department:int;

    public function EmployeeHireEvent(department:int, firstName:String, lastName:String, position:String) {
        super(HIRE, false, false);

        _department = department;
        _firstName = firstName;
        _lastName = lastName;
        _position = position;
    }

    public function get lastName():String {
        return _lastName;
    }

    public function get firstName():String {
        return _firstName;
    }

    public function get position():String {
        return _position;
    }

    public function get department():int {
        return _department;
    }

    override public function clone():Event {
        return new EmployeeHireEvent(department, firstName, lastName, position);
    }
}
}
