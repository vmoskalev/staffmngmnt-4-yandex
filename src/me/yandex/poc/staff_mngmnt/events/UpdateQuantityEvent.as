/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 10:21
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

import me.yandex.poc.staff_mngmnt.entities.api.IEmployeesCounter;

public class UpdateQuantityEvent extends Event {
    public static const UPDATE_QUANTITY_EVERYWHERE:String = "update_quantity_everywhere";
    public static const UPDATE_QUANTITY_IN_DEPARTMENT:String = "update_quantity_in_department";
    public static const UPDATE_QUANTITY_IN_POSITION:String = "update_quantity_in_position";

    private var _nID:int;

    public function UpdateQuantityEvent(type:String, nID:int) {
        super(type, false, false);
        _nID = nID;
    }

    public function get nID():int {
        return _nID;
    }

    override public function clone():Event {
        return new UpdateQuantityEvent(type, nID);
    }
}
}
