/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 11.08.13
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

import me.yandex.poc.staff_mngmnt.entities.api.IDepartment;

import mx.collections.IList;

public class TransferEvent extends Event {
    public static const EMPLOYEES_TRANSFER:String = "do_employees_actual_transfer";

    private var _nDeptID:int;

    public function TransferEvent(nDeptID:int) {
        super(EMPLOYEES_TRANSFER, false, false);
        _nDeptID = nDeptID;
    }

    public function get nDeptID():int {
        return _nDeptID;
    }

    override public function clone():Event {
        return new TransferEvent(nDeptID);
    }
}
}
