/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 22.08.13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class ShowDialogEvent extends Event {
    public static const SHOW_DIALOG:String = "show_dialog_event";

    private var _dialogClass:Class;

    public function get dialogClass():Class {
        return _dialogClass;
    }

    public function ShowDialogEvent(dialogClass:Class, type:String = SHOW_DIALOG) {
        super(type, false, false);
        _dialogClass = dialogClass;
    }

    override public function clone():Event {
        return new ShowDialogEvent(dialogClass, type);
    }
}
}
