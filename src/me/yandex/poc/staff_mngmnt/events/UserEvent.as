/**
 * Created with IntelliJ IDEA.
 * User: vladimir
 * Date: 03.08.13
 * Time: 16:46
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class UserEvent extends Event {
    public static const MAIN_VIEW_READY:String = "user_main_view_ready";
    public static const APPLICATION_CLOSING:String = "user_application_close";

    public static const PROCEED_TO_APPLICATION:String = "user_proceed_to_app";
    public static const BROWSE_FOR_DATABASE:String = "user_browse_for_db";

    public function UserEvent(type:String) {
        super(type, false, false);
    }

    override public function clone():Event {
        return new UserEvent(type);
    }
}
}
