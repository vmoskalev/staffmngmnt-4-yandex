/**
 * Created with IntelliJ IDEA.
 * User: Vladimir
 * Date: 20.08.13
 * Time: 18:04
 * To change this template use File | Settings | File Templates.
 */
package me.yandex.poc.staff_mngmnt.events {
import flash.events.Event;

public class SQLServiceResultEvent extends Event {
    private var _result:Array;
    private var _token:String;

    public function SQLServiceResultEvent(type:String, result:Array, token:String = "") {
        super(type, false, false);
        _result = result;
        _token = token;
    }

    public function get result():Array {
        return _result;
    }

    public function get token():String {
        return _token;
    }

    override public function clone():Event {
        return new SQLServiceResultEvent(type, result, token);
    }
}
}
